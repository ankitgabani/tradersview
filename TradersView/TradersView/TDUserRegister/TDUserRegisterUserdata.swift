//
//	TDUserRegisterUserdata.swift
//
//	Create by mac on 20/1/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class TDUserRegisterUserdata : NSObject, NSCoding{

	var id : String!
	var acType : String!
	var accuracy : String!
	var commentPer : String!
	var coverImg : String!
	var createdAt : String!
	var deviceToken : String!
	var deviceType : String!
	var email : String!
	var emailVerify : String!
	var facebookId : String!
	var followers : String!
	var following : String!
	var googleId : String!
	var isBan : String!
	var isChange : String!
	var isPlan : String!
	var isPolice : String!
	var mentionPer : String!
	var mobileNo : String!
	var mpin : String!
	var name : String!
	var otp : String!
	var password : String!
	var profileImg : String!
	var status : String!
	var tagPer : String!
	var updatedAt : String!
	var username : String!
    var is_premium: String!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
        is_premium = dictionary["is_premium"] as? String == nil ? "" : dictionary["is_premium"] as? String

		id = dictionary["_id"] as? String == nil ? "" : dictionary["_id"] as? String
		acType = dictionary["ac_type"] as? String == nil ? "" : dictionary["ac_type"] as? String
		accuracy = dictionary["accuracy"] as? String == nil ? "" : dictionary["accuracy"] as? String
		commentPer = dictionary["comment_per"] as? String == nil ? "" : dictionary["comment_per"] as? String
		coverImg = dictionary["cover_img"] as? String == nil ? "" : dictionary["cover_img"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		deviceToken = dictionary["device_token"] as? String == nil ? "" : dictionary["device_token"] as? String
		deviceType = dictionary["device_type"] as? String == nil ? "" : dictionary["device_type"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		emailVerify = dictionary["email_verify"] as? String == nil ? "" : dictionary["email_verify"] as? String
		facebookId = dictionary["facebook_id"] as? String == nil ? "" : dictionary["facebook_id"] as? String
		followers = dictionary["followers"] as? String == nil ? "" : dictionary["followers"] as? String
		following = dictionary["following"] as? String == nil ? "" : dictionary["following"] as? String
		googleId = dictionary["google_id"] as? String == nil ? "" : dictionary["google_id"] as? String
		isBan = dictionary["is_ban"] as? String == nil ? "" : dictionary["is_ban"] as? String
		isChange = dictionary["is_change"] as? String == nil ? "" : dictionary["is_change"] as? String
		isPlan = dictionary["is_plan"] as? String == nil ? "" : dictionary["is_plan"] as? String
		isPolice = dictionary["is_police"] as? String == nil ? "" : dictionary["is_police"] as? String
		mentionPer = dictionary["mention_per"] as? String == nil ? "" : dictionary["mention_per"] as? String
		mobileNo = dictionary["mobile_no"] as? String == nil ? "" : dictionary["mobile_no"] as? String
		mpin = dictionary["mpin"] as? String == nil ? "" : dictionary["mpin"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		otp = dictionary["otp"] as? String == nil ? "" : dictionary["otp"] as? String
		password = dictionary["password"] as? String == nil ? "" : dictionary["password"] as? String
		profileImg = dictionary["profile_img"] as? String == nil ? "" : dictionary["profile_img"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		tagPer = dictionary["tag_per"] as? String == nil ? "" : dictionary["tag_per"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
		username = dictionary["username"] as? String == nil ? "" : dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        if is_premium != nil{
            dictionary["is_premium"] = is_premium
        }
		if id != nil{
			dictionary["_id"] = id
		}
		if acType != nil{
			dictionary["ac_type"] = acType
		}
		if accuracy != nil{
			dictionary["accuracy"] = accuracy
		}
		if commentPer != nil{
			dictionary["comment_per"] = commentPer
		}
		if coverImg != nil{
			dictionary["cover_img"] = coverImg
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if deviceToken != nil{
			dictionary["device_token"] = deviceToken
		}
		if deviceType != nil{
			dictionary["device_type"] = deviceType
		}
		if email != nil{
			dictionary["email"] = email
		}
		if emailVerify != nil{
			dictionary["email_verify"] = emailVerify
		}
		if facebookId != nil{
			dictionary["facebook_id"] = facebookId
		}
		if followers != nil{
			dictionary["followers"] = followers
		}
		if following != nil{
			dictionary["following"] = following
		}
		if googleId != nil{
			dictionary["google_id"] = googleId
		}
		if isBan != nil{
			dictionary["is_ban"] = isBan
		}
		if isChange != nil{
			dictionary["is_change"] = isChange
		}
		if isPlan != nil{
			dictionary["is_plan"] = isPlan
		}
		if isPolice != nil{
			dictionary["is_police"] = isPolice
		}
		if mentionPer != nil{
			dictionary["mention_per"] = mentionPer
		}
		if mobileNo != nil{
			dictionary["mobile_no"] = mobileNo
		}
		if mpin != nil{
			dictionary["mpin"] = mpin
		}
		if name != nil{
			dictionary["name"] = name
		}
		if otp != nil{
			dictionary["otp"] = otp
		}
		if password != nil{
			dictionary["password"] = password
		}
		if profileImg != nil{
			dictionary["profile_img"] = profileImg
		}
		if status != nil{
			dictionary["status"] = status
		}
		if tagPer != nil{
			dictionary["tag_per"] = tagPer
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        is_premium = aDecoder.decodeObject(forKey: "is_premium") as? String

         id = aDecoder.decodeObject(forKey: "_id") as? String
         acType = aDecoder.decodeObject(forKey: "ac_type") as? String
         accuracy = aDecoder.decodeObject(forKey: "accuracy") as? String
         commentPer = aDecoder.decodeObject(forKey: "comment_per") as? String
         coverImg = aDecoder.decodeObject(forKey: "cover_img") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         deviceToken = aDecoder.decodeObject(forKey: "device_token") as? String
         deviceType = aDecoder.decodeObject(forKey: "device_type") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         emailVerify = aDecoder.decodeObject(forKey: "email_verify") as? String
         facebookId = aDecoder.decodeObject(forKey: "facebook_id") as? String
         followers = aDecoder.decodeObject(forKey: "followers") as? String
         following = aDecoder.decodeObject(forKey: "following") as? String
         googleId = aDecoder.decodeObject(forKey: "google_id") as? String
         isBan = aDecoder.decodeObject(forKey: "is_ban") as? String
         isChange = aDecoder.decodeObject(forKey: "is_change") as? String
         isPlan = aDecoder.decodeObject(forKey: "is_plan") as? String
         isPolice = aDecoder.decodeObject(forKey: "is_police") as? String
         mentionPer = aDecoder.decodeObject(forKey: "mention_per") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         mpin = aDecoder.decodeObject(forKey: "mpin") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         otp = aDecoder.decodeObject(forKey: "otp") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         profileImg = aDecoder.decodeObject(forKey: "profile_img") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         tagPer = aDecoder.decodeObject(forKey: "tag_per") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
        if is_premium != nil{
            aCoder.encode(is_premium, forKey: "is_premium")
        }
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if acType != nil{
			aCoder.encode(acType, forKey: "ac_type")
		}
		if accuracy != nil{
			aCoder.encode(accuracy, forKey: "accuracy")
		}
		if commentPer != nil{
			aCoder.encode(commentPer, forKey: "comment_per")
		}
		if coverImg != nil{
			aCoder.encode(coverImg, forKey: "cover_img")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deviceToken != nil{
			aCoder.encode(deviceToken, forKey: "device_token")
		}
		if deviceType != nil{
			aCoder.encode(deviceType, forKey: "device_type")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if emailVerify != nil{
			aCoder.encode(emailVerify, forKey: "email_verify")
		}
		if facebookId != nil{
			aCoder.encode(facebookId, forKey: "facebook_id")
		}
		if followers != nil{
			aCoder.encode(followers, forKey: "followers")
		}
		if following != nil{
			aCoder.encode(following, forKey: "following")
		}
		if googleId != nil{
			aCoder.encode(googleId, forKey: "google_id")
		}
		if isBan != nil{
			aCoder.encode(isBan, forKey: "is_ban")
		}
		if isChange != nil{
			aCoder.encode(isChange, forKey: "is_change")
		}
		if isPlan != nil{
			aCoder.encode(isPlan, forKey: "is_plan")
		}
		if isPolice != nil{
			aCoder.encode(isPolice, forKey: "is_police")
		}
		if mentionPer != nil{
			aCoder.encode(mentionPer, forKey: "mention_per")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if mpin != nil{
			aCoder.encode(mpin, forKey: "mpin")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if otp != nil{
			aCoder.encode(otp, forKey: "otp")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if profileImg != nil{
			aCoder.encode(profileImg, forKey: "profile_img")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if tagPer != nil{
			aCoder.encode(tagPer, forKey: "tag_per")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
