//
//	TDUserRegister.swift
//
//	Create by mac on 20/1/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class TDUserRegister : NSObject, NSCoding{

	var messages : String!
	var status : Int!
	var userdata : [TDUserRegisterUserdata]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		messages = dictionary["messages"] as? String == nil ? "" : dictionary["messages"] as? String
		status = dictionary["status"] as? Int == nil ? 0 : dictionary["status"] as? Int
		userdata = [TDUserRegisterUserdata]()
		if let userdataArray = dictionary["userdata"] as? [NSDictionary]{
			for dic in userdataArray{
				let value = TDUserRegisterUserdata(fromDictionary: dic)
				userdata.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if messages != nil{
			dictionary["messages"] = messages
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userdata != nil{
			var dictionaryElements = [NSDictionary]()
			for userdataElement in userdata {
				dictionaryElements.append(userdataElement.toDictionary())
			}
			dictionary["userdata"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         messages = aDecoder.decodeObject(forKey: "messages") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         userdata = aDecoder.decodeObject(forKey: "userdata") as? [TDUserRegisterUserdata]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if messages != nil{
			aCoder.encode(messages, forKey: "messages")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if userdata != nil{
			aCoder.encode(userdata, forKey: "userdata")
		}

	}

}