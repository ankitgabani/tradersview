//
//  AppDelegate.swift
//  TradersView
//
//  Created by Ajeet Sharma on 17/10/21.
//

import UIKit
import GoogleSignIn
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var mainNavigation:UINavigationController?
    
    var loginResponseData:TDUserRegisterUserdata?
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Use Firebase library to configure APIs
        GIDSignIn.sharedInstance().clientID = "477146900600-j15835agol5sg82r8j2arur69r2gmd24.apps.googleusercontent.com"
        return true
    }
    
    override init() {
        super.init()
            FirebaseApp.configure()
    }

    func saveCurrentUserData(dic: TDUserRegisterUserdata)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "CURRENT_USER_DATA")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> TDUserRegisterUserdata
    {
        if let data = UserDefaults.standard.object(forKey: "CURRENT_USER_DATA"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! TDUserRegisterUserdata
        }
        return TDUserRegisterUserdata()
    }

}

