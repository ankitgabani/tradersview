//
//  TMViewController.swift
//  TradersView
//
//  Created by Ankit Gabani on 04/02/22.
//

import UIKit

class TMViewController: MasterViewController {

    @IBOutlet weak var imgTop: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDes: UITextView!
    
    var post_ID = "6"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        callApiToFetchUserProfile()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func callApiToFetchUserProfile(){
        
        let request = GetPageByID(_id: self.post_ID ?? "")
        
        ApiCallManager.shared.apiCall(request: request, apiType: .GET_PAGE_BY_ID, responseType: GetPostByIDResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                
                if let userData = results.data{
                    
                    
                    DispatchQueue.main.async {
                        
                        self.imgTop.sd_setImage(with: URL(string: "\(userData.images ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
                        
                        self.lblTitle.text = userData.name?.uppercased() ?? ""
                        
                        self.txtDes.text = userData.descriptions?.html2String
                        
                    }
                    
                }
                else{
                    
                    self.showAlertPopupWithMessageWithHandler(msg: "Data is not available for this user") {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
                
                
            }
            else {
                
                self.showAlertPopupWithMessageWithHandler(msg: results.messages) {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            
            
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
    
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

@IBDesignable
extension UITextField {

    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}
