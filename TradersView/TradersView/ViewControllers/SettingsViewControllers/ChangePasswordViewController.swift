//
//  ChangePasswordViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 08/11/21.
//

import UIKit

class ChangePasswordViewController: MasterViewController {
    
    @IBOutlet weak var currentPasswordTextfield: UITextField!
    
    @IBOutlet weak var newPasswordTextfield: UITextField!
    
    @IBOutlet weak var reenterNewPasswordTextfield: UITextField!
    
    //MARK:- UIViewcontroller lifecycle methods -----
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentPasswordTextfield.delegate = self
        newPasswordTextfield.delegate = self
        reenterNewPasswordTextfield.delegate = self
    }
    
    
    //MARK:- UIButton action methods ----
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func changePasswordButtonAction(_ sender: Any) {
        
        if self.isTextfieldEmpty(textFields: [self.currentPasswordTextfield, self.newPasswordTextfield, self.reenterNewPasswordTextfield])
        {
            return
        }
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            
            self.apiCallPasswordChange(userId: (appDelegate.loginResponseData?.id)!)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
        
    }
    
    //MARK:- Api call methods ---
    
    func apiCallPasswordChange(userId:String){
        
        let request = ResetPasswordRequest(_id: userId, _old_password: self.currentPasswordTextfield.text!, _new_password: self.newPasswordTextfield.text!, _confirm_password: self.reenterNewPasswordTextfield.text!)
        
        ApiCallManager.shared.apiCall(request: request, apiType: .RESET_PASSWORD, responseType: ChangePasswordResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1{
                
                
                DispatchQueue.main.async {
                    
                    self.showAlertPopupWithMessage(msg: results.messages)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
            else{
                
                self.showAlertPopupWithMessage(msg: results.messages)
                
            }
            
        } failureHandler: { (error) in
            
            self.showErrorMessage(error: error)
        }
    }
    
}

extension ChangePasswordViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
    
    
}
