//
//  SettingsViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 25/10/21.
//

import UIKit
import SDWebImage
import AVFoundation
import UITextView_Placeholder
import SVProgressHUD

class SettingsViewController: MasterViewController {
    
    //MARK: - @IBOutlet
    
    @IBOutlet weak var viewNewUserName: UIView!
    @IBOutlet weak var txtEnterNewUserName: UITextField!
    
    @IBOutlet weak var viewSubscription: UIView!
        
    @IBOutlet weak var viewConatctPopup: UIView!
    @IBOutlet weak var txtEnterName: UITextField!
    @IBOutlet weak var txtConactNumber: UITextField!
    @IBOutlet weak var txtEnterEmail: UITextField!
    
    @IBOutlet weak var imgProfileConat: UIImageView!
    @IBOutlet weak var txtEnterMessafe: UITextView!
    
    
    
    @IBOutlet weak var tableViewSettings: UITableView!
    @IBOutlet weak var collectionViewComments: UICollectionView!
    @IBOutlet weak var collectionViewTag: UICollectionView!
    @IBOutlet weak var collectionViewMention: UICollectionView!
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var switchPushNotification: UISwitch!
    @IBOutlet weak var switchDarkMode: UISwitch!
    @IBOutlet weak var switchPrivateAccount: UISwitch!
    
    @IBOutlet weak var lblUserNAme: UILabel!
    @IBOutlet weak var lblLastUpdateTime: UILabel!
    
    var currentUserId:String?
    
    var selectedComments = -1
    var selectedTag = -1
    var selectedMention = -1
    
    var flowLayoutTrend: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 100, height: 35)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 12
        return _flowLayout
    }
    
    var userProfileObj:GetProfileByIDDatum?
    
    
    var arrName = ["Follower","Public","Custom"]
    
    //MARK: - UIViewcontroller lifecycle methods ----
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewConatctPopup.isHidden = true
        viewSubscription.isHidden = true
        viewNewUserName.isHidden = true
        
        txtEnterMessafe.placeholder = "Enter the Message?"
        txtEnterMessafe.placeholderColor = UIColor.darkGray
        
        collectionViewComments.delegate = self
        collectionViewComments.dataSource = self
        self.collectionViewComments.collectionViewLayout = flowLayoutTrend
        
        collectionViewTag.delegate = self
        collectionViewTag.dataSource = self
        self.collectionViewTag.collectionViewLayout = flowLayoutTrend
        
        collectionViewMention.delegate = self
        collectionViewMention.dataSource = self
        self.collectionViewMention.collectionViewLayout = flowLayoutTrend
        
        self.tableViewSettings.estimatedRowHeight = 88.0
        self.tableViewSettings.rowHeight = UITableView.automaticDimension
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id ?? ""
            
        }
        else
        {
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            callApiToFetchUserProfile()
        }
    }
    
    //MARK: - Action Method
    
    @IBAction func clickedPrivateAccount(_ sender: UISwitch) {
        
        if let userID = self.currentUserId{
            
            if sender.isOn == true
            {
                CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "", mention_per_No: "", ac_type: "2")
            }
            else
            {
                CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "", mention_per_No: "", ac_type: "1")

            }
            
        }
        else
        {
            self.showAlertPopupWithMessage(msg: "User id is not available")
        }
        
    }
    
    
    @IBAction func clickedUpdateUsernaem(_ sender: Any) {
                
        if self.isTextfieldEmpty(textFields: [self.txtEnterNewUserName]){
            return
        }
        
        if self.txtEnterNewUserName.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            self.showAlertPopupWithMessage(msg: "Please enter user name")
            return
        }
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            
            self.updateUserNameApi(userID: appDelegate.loginResponseData?.id ?? "")
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
        
    }
    
    
    @IBAction func clickedCheckout(_ sender: Any) {
        viewSubscription.isHidden = true
    }
    
    @IBAction func clickedCancelConatcPopup(_ sender: Any) {
        viewConatctPopup.isHidden = true
    }
    
    @IBAction func clickedSendconacyPopu(_ sender: Any) {
        
        if self.isTextfieldEmpty(textFields: [self.txtEnterName, self.txtConactNumber, self.txtEnterEmail]){
            
            return
            
        }
        
        if self.txtEnterMessafe.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            
            self.showAlertPopupWithMessage(msg: "Please enter message")
            return
            
        }
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            
            self.contactUsApi(userID: appDelegate.loginResponseData?.id ?? "")
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
        
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedChoosePhot(_ sender: Any)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:-- ImagePicker delegate
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            
            imgProfile.image = pickedImage
            
            let request = UpdateCommnePerRequest(_user_id: currentUserId!, comment_per: "", tag_per: "", mention_per: "",ac_type: "")

            self.apiCallPostWithImage(postImage: pickedImage, requestPost: request)
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedUpdateUsername(_ sender: Any) {
        viewNewUserName.isHidden = false
    }
    
    @IBAction func clickedChangePassword(_ sender: Any) {
        
        if let userID = self.currentUserId{
            
            self.pushScreenWithScreenName(screenName: "ChangePasswordViewController", currentUserId: userID)
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
        }
        
    }
    
    @IBAction func clickedSetTwoFactor(_ sender: Any) {
    }
    
    @IBAction func clickedBlockedAccount(_ sender: Any) {
        self.showBlockAndMuteScreen(isBLock: true)
    }
    
    @IBAction func clickedMuteUser(_ sender: Any) {
        self.showBlockAndMuteScreen(isBLock: false)
    }
    
    @IBAction func clickedPostBanYou(_ sender: Any) {
        
    }
    
    @IBAction func clickedUserBanYou(_ sender: Any) {
        
    }
    
    @IBAction func clickedFavouriteUsers(_ sender: Any) {
        
        if let userID = self.currentUserId{
            
            let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
            let vc = dashBoardStoryBoard.instantiateViewController(identifier: "FavoriteUserProfileViewController") as! FavoriteUserProfileViewController
            vc.isFromFollowList = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
    }
    
    @IBAction func clickedFollowRequest(_ sender: Any) {
        if let userID = self.currentUserId{
            
            let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
            let vc = dashBoardStoryBoard.instantiateViewController(identifier: "FavoriteUserProfileViewController") as! FavoriteUserProfileViewController
            vc.isFromFollowList = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
    }
    
    @IBAction func clickedConactUs(_ sender: Any) {
        
        if let userID = self.currentUserId{
            
            viewConatctPopup.isHidden = false
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
        }
    }
    
    @IBAction func clickedSPSoftTech(_ sender: Any) {
        let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let vc = dashBoardStoryBoard.instantiateViewController(identifier: "TMViewController") as! TMViewController
        vc.post_ID = "5"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedPP(_ sender: Any) {
        let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let vc = dashBoardStoryBoard.instantiateViewController(identifier: "TMViewController") as! TMViewController
        vc.post_ID = "6"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedTP(_ sender: Any) {
        let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let vc = dashBoardStoryBoard.instantiateViewController(identifier: "TMViewController") as! TMViewController
        vc.post_ID = "7"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedSubscription(_ sender: Any) {
        viewSubscription.isHidden = false
    }
    
    @IBAction func clickedAppVersion(_ sender: Any) {
    }
    
    @IBAction func clickedRateApp(_ sender: Any) {
        AppUse.rateApp()
    }
    
    @IBAction func clickedPrivacyPolicy(_ sender: Any) {
        let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let vc = dashBoardStoryBoard.instantiateViewController(identifier: "TMViewController") as! TMViewController
        vc.post_ID = "6"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedAds(_ sender: Any) {
    }
    
    @IBAction func clickedVerifyMe(_ sender: Any) {
    }
    
    @IBAction func clickedLogut(_ sender: Any) {
        
        self.areYouSureAlertPopup(title: "Logout?", msg: "Are you sure for logout?") {
            
            self.callLogOutApi()
            
        } noHandler: {
            
        }
        
    }
    
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedMyProfileTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: UserProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: MessageTabViewController = mainStoryboard.instantiateViewController(withIdentifier: "MessageTabViewController") as! MessageTabViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedProfileSetting(_ sender: Any) {
    }
    
    @IBAction func clickedCratePost(_ sender: Any) {
    }

    //MARK: - API Call
    
    
    func apiCallPostWithImage(postImage:UIImage, requestPost:UpdateCommnePerRequest){
        
        SVProgressHUD.show()
        
        ApiCallManager.shared.UploadImageUpload(request: requestPost, apiType: .UPDATE_PROFILE, uiimagePic: postImage, responseType: ContactusResponse.self) { (response) in
            
            let result:ContactusResponse = response
            print("Result - \(result.messages) -- ")
            
            if result.status == 1 {
                
                SVProgressHUD.dismiss()
                
                DispatchQueue.main.async {
                    self.view.makeToast(result.messages)
                }
                
            }
            else{
                
                SVProgressHUD.dismiss()
                self.showAlertPopupWithMessage(msg: result.messages)
                
            }
            
        } failureHandler: { (error) in
            SVProgressHUD.dismiss()
            self.showErrorMessage(error: error)
            
        }
        
    }
    
    func CommenteUpdateApi(userID:String,comment_per_No: String,tag_per_No: String,mention_per_No: String,ac_type: String)
    {
        
        let request = UpdateCommnePerRequest(_user_id: userID, comment_per: comment_per_No, tag_per: tag_per_No, mention_per: mention_per_No,ac_type: ac_type)
                
        ApiCallManager.shared.apiCall(request: request, apiType: .UPDATE_PROFILE, responseType: ContactusResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                                
                DispatchQueue.main.async {
                    
                    self.view.makeToast( results.messages)
                }
                
            }
            else {
                
                self.showAlertPopupWithMessage(msg: results.messages)
            }
            
        } failureHandler: { (error) in
            self.showErrorMessage(error: error)
            
        }
    }
    
    func contactUsApi(userID:String)
    {
        
        let request = ContactRequest(_user_id: userID, _name: self.txtEnterName.text!, _phone: self.txtConactNumber.text!, _email: self.txtEnterEmail.text!, _msg: self.txtEnterMessafe.text!)
                
        ApiCallManager.shared.apiCall(request: request, apiType: .CONTACT_US, responseType: ContactusResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                
                self.showAlertPopupWithMessageWithHandler(msg: results.messages) {
                    
                    DispatchQueue.main.async {
                        
                        self.viewConatctPopup.isHidden = true
                    }
                }
            }
            else {
                
                self.showAlertPopupWithMessage(msg: results.messages)
            }
            
        } failureHandler: { (error) in
            self.showErrorMessage(error: error)
            
        }
    }
    
    func updateUserNameApi(userID:String)
    {
        
        let request = UpdateUserNnameRequest(_user_id: userID, username: self.txtEnterNewUserName.text!)
        
        ApiCallManager.shared.apiCall(request: request, apiType: .UPDATE_USER_NAME, responseType: ContactusResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                
                self.showAlertPopupWithMessageWithHandler(msg: results.messages) {
                    
                    DispatchQueue.main.async {
                        
                        self.viewNewUserName.isHidden = true
                        
                        self.lblUserNAme.text = self.txtEnterNewUserName.text
                        self.txtEnterNewUserName.text = self.txtEnterNewUserName.text
                        self.txtEnterName.text = self.txtEnterNewUserName.text
                    }
                }
            }
            else {
                
                self.showAlertPopupWithMessage(msg: results.messages)
            }
            
        } failureHandler: { (error) in
            self.showErrorMessage(error: error)
            
        }
        
    }
    
    
    func callApiToFetchUserProfile(){
        
        let request = GetprofileByIdRequest(_user_id: self.currentUserId ?? "", _id: self.currentUserId ?? "")
        
        
        ApiCallManager.shared.apiCall(request: request, apiType: .GET_PROFILE_BY_ID, responseType: GetProfileByIDResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                
                if let userData = results.data{
                    
                    self.userProfileObj = userData
                    
                    DispatchQueue.main.async {
                        
                        self.imgProfileConat.sd_setImage(with: URL(string: "\(userData.profileImg ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
                        
                        self.imgProfile.sd_setImage(with: URL(string: "\(userData.profileImg ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
                        
                        self.lblUserNAme.text = userData.username ?? ""
                        self.txtEnterNewUserName.text = userData.username ?? ""
                        self.txtEnterName.text = userData.username ?? ""
                        self.txtEnterEmail.text = userData.email ?? ""
                        self.txtConactNumber.text = userData.mobileNo ?? ""
                        
                        if userData.commentPerType == "1"
                        {
                            self.selectedComments = 1
                        }
                        else if userData.commentPerType == "2"
                        {
                            self.selectedComments = 0
                        }
                        else
                        {
                            self.selectedComments = 2
                        }
                        
                        if userData.tagPerType == "1"
                        {
                            self.selectedTag = 1
                        }
                        else if userData.tagPerType == "2"
                        {
                            self.selectedTag = 0
                        }
                        else
                        {
                            self.selectedTag = 2
                        }
                        
                        if userData.mentionPerType == "1"
                        {
                            self.selectedMention = 1
                        }
                        else if userData.mentionPerType == "2"
                        {
                            self.selectedMention = 0
                        }
                        else
                        {
                            self.selectedMention = 2
                        }
                        
                        if userData.acType == "1"
                        {
                            self.switchPrivateAccount.isOn = false
                        }
                        else
                        {
                            self.switchPrivateAccount.isOn = true
                        }
                        
                        
                        self.collectionViewMention.reloadData()
                        self.collectionViewComments.reloadData()
                        self.collectionViewTag.reloadData()
                    }
                    
                }
                else{
                    
                    self.showAlertPopupWithMessageWithHandler(msg: "Data is not available for this user") {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
                
                
            }
            else {
                
                self.showAlertPopupWithMessageWithHandler(msg: results.messages) {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            
            
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
    
    func callLogOutApi(){
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            
            let logoutRequest = LogoutRequest(id: appDelegate.loginResponseData?.id ?? "")
            
            ApiCallManager.shared.apiCall(request: logoutRequest, apiType: .LOGOUT, responseType: LogoutResponse.self, requestMethod:.POST) { (results) in
                
                
                let logoutResonse:LogoutResponse = results
                
                if logoutResonse.status != 1 {
                    self.showAlertPopupWithMessage(msg: logoutResonse.messages)
                }
                else{
                    
                    UserDefaults.standard.setValue(false, forKey: "UserLogin")
                    UserDefaults.standard.synchronize()
                    
                    self.showAlertPopupWithMessageWithHandler(msg: "Logout Successfully Successfully!!") {
                        
                        self.appDelegate.loginResponseData = nil
                        
                        self.appDelegate.saveCurrentUserData(dic: TDUserRegisterUserdata())
                        self.appDelegate.loginResponseData = TDUserRegisterUserdata()
                                                
                        UserDefaults.standard.setValue(nil, forKey: Constants.USER_DEFAULT_KEY_USER_DATA)
                        
                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let home: SplashViewController = mainStoryboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                        let homeNavigation = UINavigationController(rootViewController: home)
                        homeNavigation.navigationBar.isHidden = true
                        self.appDelegate.window?.rootViewController = homeNavigation
                        self.appDelegate.window?.makeKeyAndVisible()
                        
                    }
                    
                }
                
                
            } failureHandler: { (error) in
                
            }
            
        }
        
    }
    
}

extension SettingsViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewComments
        {
            let cell = collectionViewComments.dequeueReusableCell(withReuseIdentifier: "SettingCollectionViewCell", for: indexPath) as! SettingCollectionViewCell
            
            cell.lblName.text = arrName[indexPath.row]
            
            if selectedComments == indexPath.row
            {
                cell.mainView.backgroundColor = UIColor(red: 71/255, green: 69/255, blue: 113/255, alpha: 1)
                cell.mainView.alpha = 0.9
            }
            else
            {
                cell.mainView.backgroundColor = UIColor.darkGray
                cell.mainView.alpha = 0.7
            }
            
            return cell
        }
        else if collectionView == collectionViewTag
        {
            let cell = collectionViewTag.dequeueReusableCell(withReuseIdentifier: "SettingCollectionViewCell", for: indexPath) as! SettingCollectionViewCell
            
            cell.lblName.text = arrName[indexPath.row]
            
            if selectedTag == indexPath.row
            {
                cell.mainView.backgroundColor = UIColor(red: 71/255, green: 69/255, blue: 113/255, alpha: 1)
                cell.mainView.alpha = 0.9
            }
            else
            {
                cell.mainView.backgroundColor = UIColor.darkGray
                cell.mainView.alpha = 0.7
            }
            
            return cell
        }
        else
        {
            let cell = collectionViewMention.dequeueReusableCell(withReuseIdentifier: "SettingCollectionViewCell", for: indexPath) as! SettingCollectionViewCell
            
            cell.lblName.text = arrName[indexPath.row]
            
            if selectedMention == indexPath.row
            {
                cell.mainView.backgroundColor = UIColor(red: 71/255, green: 69/255, blue: 113/255, alpha: 1)
                cell.mainView.alpha = 0.9
            }
            else
            {
                cell.mainView.backgroundColor = UIColor.darkGray
                cell.mainView.alpha = 0.7
            }
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let userID = self.currentUserId{
            
            if collectionView == collectionViewComments
            {
                if indexPath.row == 0
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "2", tag_per_No: "", mention_per_No: "", ac_type: "")
                }
                else if indexPath.row == 1
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "1", tag_per_No: "", mention_per_No: "", ac_type: "")
                }
                else if indexPath.row == 2
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "3", tag_per_No: "", mention_per_No: "", ac_type: "")
                }
                
                selectedComments = indexPath.row
                collectionViewComments.reloadData()
            }
            else if collectionView == collectionViewTag
            {
                if indexPath.row == 0
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "2", mention_per_No: "", ac_type: "")
                }
                else if indexPath.row == 1
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "1", mention_per_No: "", ac_type: "")
                }
                else if indexPath.row == 2
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "3", mention_per_No: "", ac_type: "")
                }
                
                selectedTag = indexPath.row
                collectionViewTag.reloadData()
            }
            else
            {
                if indexPath.row == 0
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "", mention_per_No: "2", ac_type: "")
                }
                else if indexPath.row == 1
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "", mention_per_No: "1", ac_type: "")
                }
                else if indexPath.row == 2
                {
                    CommenteUpdateApi(userID: userID, comment_per_No: "", tag_per_No: "", mention_per_No: "3", ac_type: "")
                }
                
                selectedMention = indexPath.row
                collectionViewMention.reloadData()
            }
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
        
        
        
    }
    
}

struct AppUse {
    
    static let appID = "1549378669"
    
    static func rateApp(){
        if let url = URL(string:"itms-apps://itunes.apple.com/app/\(appID)") {
            AppUse.openURL(url)
        }
    }
    
    static func openURL(_ url: URL){
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static let shareText = "TradersView:- "
    
    static func shareApp(inController controller:UIViewController){
        let textToShare = "\(AppUse.shareText) \n itms-apps://itunes.apple.com/app/\(appID)"
        AppUse.itemShare(inController: controller, items: textToShare)
    }
    
    static func itemShare(inController controller:UIViewController, items:Any){
        let objectsToShare = [items]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = controller.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: 100, y: 200, width: 300, height: 300)
        controller.present(activityVC, animated: true, completion: nil)
    }
    
}
