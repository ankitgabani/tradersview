//
//  HomeHeaderView.swift
//  TradersView
//
//  Created by Ankit Gabani on 23/01/22.
//

import UIKit

class HomeHeaderView: UIView {
    
    @IBOutlet weak var btnCom: UIButton!
    @IBOutlet weak var btnMyFeed: UIButton!
    
    @IBOutlet weak var viewCom: UIView!
    @IBOutlet weak var viewMyFeed: UIView!
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    
    @IBAction func clickedCom(_ sender: Any) {
      
    }
    
    @IBAction func clickedMy(_ sender: Any) {
      
    }
    
}
