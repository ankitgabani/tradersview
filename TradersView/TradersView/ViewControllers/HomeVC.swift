//
//  HomeVC.swift
//  TradersView
//
//  Created by Ankit Gabani on 22/01/22.
//

import UIKit
import SDWebImage
import AVFoundation

class HomeVC: MasterViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, KIImagePagerDataSource,KIImagePagerImageSource {
    
    //MARK: - IBOutlet
    @IBOutlet weak var collectionViewMostPopular: UICollectionView!
    @IBOutlet weak var collectionViewTopPrpfle: UICollectionView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var arrayPopular:[MostPopularDatum] = []
    var arrayTopProfile:[TopProfileDatum] = []
    
    var arrayCommunity: [CommunityResponseDatum] = []
    
    var selectedIndex = 0
    
    let refreshControl = UIRefreshControl()
    private var currentUserId:String?
    private var myPostPageNumber:Int = 0
    private var communityPageNumber:Int = 0
    
    private var shouldStopMyPostLoadMore:Bool = false
    private var shouldStopCommunityLoadMore:Bool = false
    
    var flowLayoutTrend: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 118, height: 160)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 12)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0.0
        return _flowLayout
    }
    
    var flowLayoutToProfile: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 88, height: 100)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 12)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 5.0
        return _flowLayout
    }
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lineView.layer.applySketchShadow(color: UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha: 0.7), alpha: 1, x: 0, y: 0, blur: 10, spread: 0)
        
        collectionViewMostPopular.delegate = self
        collectionViewMostPopular.dataSource = self
        self.collectionViewMostPopular.collectionViewLayout = flowLayoutTrend
        
        collectionViewTopPrpfle.delegate = self
        collectionViewTopPrpfle.dataSource = self
        self.collectionViewTopPrpfle.collectionViewLayout = flowLayoutToProfile
        
        tblView.delegate = self
        tblView.dataSource = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.tblView.addSubview(refreshControl) // not required when using UITableViewController
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        apiCallMostPopular()
        apiCallTopProfile()
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id
            apiCallCommunity()
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    //MARK:- Refresh apiCall ----
    
    @objc func refresh(_ sender: AnyObject) {
        
        apiCallMostPopular()
        apiCallTopProfile()
        communityPageNumber = 0
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id
            apiCallCommunity()
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    //MARK: - Action Metheod
    @IBAction func clickedSearch(_ sender: Any) {
        if let userID = self.currentUserId{
            
            self.pushScreenWithScreenName(screenName: "SearchViewController", currentUserId: userID)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
    }
    
    @IBAction func clickedNotification(_ sender: Any) {
        if let userID = self.currentUserId{
            
            self.pushScreenWithScreenName(screenName: "NotificationViewController", currentUserId: userID)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
    }
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        
    }
    
    @IBAction func clickedProfileTab(_ sender: Any) {
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: UserProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: MessageTabViewController = mainStoryboard.instantiateViewController(withIdentifier: "MessageTabViewController") as! MessageTabViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedProfileSettingTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: SettingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedCreatePost(_ sender: Any) {
        let storyBoardDashboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        
        let vc:PostViewController = storyBoardDashboard.instantiateViewController(identifier: "PostViewController") as! PostViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: - collectionView Metheod
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewMostPopular
        {
            return self.arrayPopular.count
        }
        else
        {
            return self.arrayTopProfile.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewMostPopular
        {
            let cell:CollectionViewCellMostPopular = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellMostPopular", for: indexPath as IndexPath) as! CollectionViewCellMostPopular
            
            
            let popular:MostPopularDatum = self.arrayPopular[indexPath.row]
            
            print(popular.image)
            cell.popularImageView.sd_setImage(with: URL(string: "\(popular.image)"), placeholderImage: UIImage(named: Constants.DEFAULT_POST_IMAGE))
            cell.popularImageView.layer.cornerRadius = 10
            
            return cell
        }
        else
        {
            let cell:TopProfileCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopProfileCollectionCell", for: indexPath as IndexPath) as! TopProfileCollectionCell
            
            let popular:TopProfileDatum = self.arrayTopProfile[indexPath.row]
            
            print(popular.profile_img)
            cell.imgProfile.sd_setImage(with: URL(string: "\(popular.profile_img)"), placeholderImage: UIImage(named: Constants.DEFAULT_POST_IMAGE))
            
            cell.lblNAme.text = popular.username
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewMostPopular
        {
            let obj = self.arrayPopular[indexPath.row]
            
            guard let url = URL(string: obj.link) else {
                return //be safe
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        else
        {
            var profileUserId:String = ""
            
            let popular:TopProfileDatum = self.arrayTopProfile[indexPath.row]
            
            profileUserId = popular.userid
            
            self.pushUserProfileScreen(userId: profileUserId, currentUserId:self.currentUserId!)
        }
        
    }
    
    //MARK: - UITableView Metheod
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrayCommunity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPostCommunity") as! CellPost
        
        if indexPath.row >= self.arrayCommunity.count{
            return UITableViewCell()
        }
        
        let obj = self.arrayCommunity[indexPath.row]
        
        cell.nameLabel.text = obj.username
        
        cell.dateLabel.text = self.changeDateFormateToDisplay(dateString: obj.date)
        cell.profilePicImageView.sd_setImage(with: URL(string: "\(obj.profileImg)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
        cell.img_Pro.sd_setImage(with: URL(string: "\(obj.profileImg)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
        
        cell.postCaptionLabel.text = obj.message
        
        
        cell.likeCountLabel.text = obj.like
        cell.commentCountLabel.text = obj.comment
        cell.shareCountLabel.text = obj.share
        
        cell.likeImageView.tag = indexPath.row
        cell.commentImageView.tag = indexPath.row
        cell.shareImageView.tag = indexPath.row
        
        
        cell.moreInfoButton.tag = indexPath.row
        
        cell.moreInfoButton.superview!.tag = tableView.tag
        cell.likeImageView.superview!.tag = tableView.tag
        cell.commentImageView.superview!.tag = tableView.tag
        cell.shareImageView.superview!.tag = tableView.tag
        
        
        cell.profilePicImageView.tag = indexPath.row
        
        cell.profilePicImageView.superview!.tag = tableView.tag
        
        
        cell.img_Pro.tag = indexPath.row
        
        cell.img_Pro.superview!.tag = tableView.tag
        
        cell.btnWriteCommnet.tag = tableView.tag
        cell.btnWriteCommnet.addTarget(self, action: #selector(commentImageViewBtn(_:)), for: .touchUpInside)
        
        
        cell.likeImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.likeImageViewTapGesture(gesture:))))
        cell.commentImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.commentImageViewTapGesture(gesture:))))
        cell.shareImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.shareImageViewTapGesture(gesture:))))
        
        cell.postImageView.changeBorder(width: 1.0, borderColor: .lightGray, cornerRadius: 10.0)
        
        cell.img_Pro.isUserInteractionEnabled = true
        
        cell.profilePicImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profilePicImageViewTapGesture(gesture:))))
        cell.img_Pro.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profilePicImageViewTapGesture(gesture:))))
        
        cell.moreInfoButton.addTarget(self, action: #selector(self.moreInfoButtonAction(_sender:)), for: .touchUpInside)
        
        
        if obj.isComment != 0 {
            cell.commentImageView.image = UIImage(named: "comment-filled")
        }
        else{
            
            cell.commentImageView.image = UIImage(named: "comment-empty")
        }
        
        if obj.isShare != 0 {
            cell.shareImageView.image = UIImage(named: "share")
        }
        else{
            
            cell.shareImageView.image = UIImage(named: "share-post")
        }
        
        if obj.isLike != 0 {
            cell.likeImageView.image = UIImage(named: "like-filled")
        }
        else{
            
            cell.likeImageView.image = UIImage(named: "like-empty")
        }
        
        if let imgVideo = obj.imageVideo{
            
            let imgObj = imgVideo[0]
            let imgUrl = imgObj.image
            
            
            switch imgUrl {
            case .integer(let intValue):
                cell.heightPostImageView.constant = 0.0
                
            case .string(let strUrl):
                cell.heightPostImageView.constant = 270
                cell.postImageView.sd_setImage(with: URL(string: "\(strUrl)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
                
            }
            
        }
        else{
            
            cell.heightPostImageView.constant = 0.0
            
        }
        
        if self.arrayCommunity.count - 1 == indexPath.row{
            
            if !self.shouldStopCommunityLoadMore{
                
                self.communityPageNumber = self.communityPageNumber + 1
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    
                    self.apiCallCommunity()
                }
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HomeHeaderView", owner: self, options: [:])?.first as! HomeHeaderView
        
        
        headerView.btnCom.addTarget(self, action: #selector(clickedOmnCom(_:)), for: .touchUpInside)
        
        headerView.btnMyFeed.addTarget(self, action: #selector(clickedOnMyFeed(_:)), for: .touchUpInside)
        
        if selectedIndex == 0
        {
            headerView.btnCom.alpha = 1
            headerView.btnMyFeed.alpha = 0.5
            headerView.viewCom.isHidden = false
            headerView.viewMyFeed.isHidden = true
        }
        else
        {
            headerView.btnCom.alpha = 0.5
            headerView.btnMyFeed.alpha = 1
            headerView.viewCom.isHidden = true
            headerView.viewMyFeed.isHidden = false
        }
        
        return headerView
    }
    
    @objc func clickedOmnCom(_ sender: UIButton)
    {
        selectedIndex = 0
        communityPageNumber = 0
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id
            apiCallCommunity()
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    @objc func clickedOnMyFeed(_ sender: UIButton)
    {
        selectedIndex = 1
        communityPageNumber = 0
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id
            apiCallCommunity()
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    //MARK: - API Calling
    func apiCallMostPopular(){
        ApiCallManager.shared.apiCall(request: ApiRequestModel(), apiType: .MOST_POPULAR, responseType: MostPopularResponse.self, requestMethod: .GET) { (results) in
            
            let response:MostPopularResponse = results
            
            if response.status == 0{
                
                self.showAlertPopupWithMessage(msg: response.messages)
                
            }
            else{
                
                if let popularList = response.data{
                    self.arrayPopular = popularList
                }
                
                DispatchQueue.main.async {
                    self.collectionViewMostPopular.reloadData()
                }
                
            }
            
        } failureHandler: { (error) in
            
            self.showErrorMessage(error: error)
        }
    }
    
    func apiCallTopProfile(){
        ApiCallManager.shared.apiCall(request: ApiRequestModel(), apiType: .TOP_PROFILE, responseType: TopProfileResponse.self, requestMethod: .GET) { (results) in
            
            let response:TopProfileResponse = results
            
            if response.status == 0{
                
                self.showAlertPopupWithMessage(msg: response.messages)
                
            }
            else{
                
                if let popularList = response.data{
                    self.arrayTopProfile = popularList
                }
                
                DispatchQueue.main.async {
                    self.collectionViewTopPrpfle.reloadData()
                }
                
            }
            
        } failureHandler: { (error) in
            
            self.showErrorMessage(error: error)
        }
    }
    
    
    func apiCallCommunity()
    {
        
        let request = CommunityRequest(_user_id: self.currentUserId!, _page: self.communityPageNumber)
        
        print("self.communityPageNumber - \(self.communityPageNumber)")
        
        ApiCallManager.shared.apiCall(request: request, apiType: .COMMUNITY, responseType: CommunityResponse.self, requestMethod: .POST) { (results) in
            
            
            DispatchQueue.main.async {
                
                self.refreshControl.endRefreshing()
            }
            
            if results.status == 1{
                
                if let data = results.data{
                    
                    if self.communityPageNumber == 0 {
                        
                        self.shouldStopCommunityLoadMore = false
                        self.arrayCommunity = data
                    }
                    else{
                        
                        self.arrayCommunity.append(contentsOf: data)
                        
                    }
                    
                }
                else{
                    
                    if self.communityPageNumber == 0 {
                        
                        self.arrayCommunity.removeAll()
                        
                    }
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.tblView.reloadData()
                }
            }
            else {
                
                if self.communityPageNumber == 0 {
                    
                    //   self.arrayMyPost.removeAll()
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                    // self.showAlertPopupWithMessage(msg: results.messages)
                    
                }
                else{
                    
                    self.shouldStopCommunityLoadMore = true
                    
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            DispatchQueue.main.async {
                
                self.refreshControl.endRefreshing()
            }
            self.showErrorMessage(error: error)
            
        }
        
    }
    
    func apiCallPostshare(postID:String){
        
        if let currentUserID = self.currentUserId{
            
            let request = SharePostActionRequest(_user_id: currentUserID, post_id: postID)
            
            ApiCallManager.shared.apiCall(request: request, apiType: .SHARE_POST, responseType: MuteActionResponse.self, requestMethod: .POST) { (results) in
                
                if results.status == 1{
                    
                }
                else if results.status == 0 {
                    
                    self.showAlertPopupWithMessage(msg: results.messages)
                }
                
            } failureHandler: { (error) in
                
                self.showErrorMessage(error: error)
            }
            
        }
        
    }
    
    func apiCallPostDelete(postID:String){
        
        if let currentUserID = self.currentUserId{
            
            let request = DeletePostActionRequest(id: postID, status: "0")
            
            ApiCallManager.shared.apiCall(request: request, apiType: .DELETE_POST, responseType: MuteActionResponse.self, requestMethod: .POST) { (results) in
                
                if results.status == 1{
                    self.apiCallCommunity()
                    
                }
                else if results.status == 0 {
                    
                    self.showAlertPopupWithMessage(msg: results.messages)
                }
                
            } failureHandler: { (error) in
                
                self.showErrorMessage(error: error)
            }
            
        }
        
    }
    
    func array(withImages pager: KIImagePager!) -> [Any]! {
        
        print("mahesh",self.arrayCommunity);
        
        
        //            if let imageArr =  (arrayPassDatasaas[0] as AnyObject) .value(forKey: "image") as? String
        //            {
        //                return imageArr.components(separatedBy: ",") as [AnyObject]
        //            }
        
        
        
        return ["https://spsofttech.com/projects/treader/images/post/667501638185749.png","https://spsofttech.com/projects/treader/images/post/956811638185749.png","https://spsofttech.com/projects/treader/images/post/463221638185749.png","https://spsofttech.com/projects/treader/images/post/841031638185749.mp4" as AnyObject]
        
    }
    
    
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleAspectFill
        
        
    }
    
    
    
    func captionForImage(at index: Int, in pager: KIImagePager?) -> String? {
        
        if (index == 3){
            //                video_show.isHidden = false
            //                print("123")
            //                let videoURL = URL(string: "https://spsofttech.com/projects/treader/images/post/841031638185749.mp4")
            //                let player = AVPlayer(url: videoURL!)
            //                let playerLayer = AVPlayerLayer(player: player)
            //                playerLayer.frame = self.view.bounds
            //                self.video_show.layer.addSublayer(playerLayer)
            //                player.play()
        }
        
        return [
            
            "",
            "",
            "",
            ""
        ][index]
        
        
    }
    
    
    
    func imagePager(_ imagePager: KIImagePager?, didScrollTo index: Int) {
        print(String(format: "%s %lu", #function, UInt(index)))
        
        
    }
    
    
    func image(with url: URL!, completion: KIImagePagerImageRequestBlock!) {
        
    }
    
    //MARK: - UITapgesture action methods ----
    
    
    @objc func likeImageViewTapGesture(gesture: UITapGestureRecognizer) {
        
        
        var postID:String = ""
        var notifyUserId:String = ""
        
        let postObj = self.arrayCommunity[gesture.view!.tag]
        postID = postObj.postid
        notifyUserId = postObj.userID
        
        print("post id - \(postObj.postid)")
        
        let label = gesture.view!.superview!.subviews.compactMap({$0 as? UILabel})
        
        
        self.likePostApi(_notifyUserId: notifyUserId, _postId: postID, imgLike: (gesture.view as! UIImageView?)!,countLabel:label[0])
        
        
    }
    @objc func commentImageViewBtn(_ sender: UIButton) {
        
        var postID:String = ""
        var notifyUserId:String = ""
        
        let postObj = self.arrayCommunity[sender.tag]
        
        postID = postObj.postid
        notifyUserId = postObj.userID
        
        print("post id - \(postObj.postid)")
        
        self.pushCommentScreen(postId: postID, notifyUserId: notifyUserId)
        
    }
    
    @objc func commentImageViewTapGesture(gesture: UITapGestureRecognizer) {
        
        
        var postID:String = ""
        var notifyUserId:String = ""
        
        let postObj = self.arrayCommunity[gesture.view!.tag]
        
        postID = postObj.postid
        notifyUserId = postObj.userID
        
        print("post id - \(postObj.postid)")
        
        self.pushCommentScreen(postId: postID, notifyUserId: notifyUserId)
        
    }
    @objc func shareImageViewTapGesture(gesture: UITapGestureRecognizer) {
        let postObj = self.arrayCommunity[gesture.view!.tag]
        print("post id - \(postObj.postid)")
        
        let dashBoardStoryBoard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let vc:SharePostViewController = dashBoardStoryBoard.instantiateViewController(identifier: "SharePostViewController")
        
        vc.arrayCommunity = postObj
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    @objc func profilePicImageViewTapGesture(gesture:UITapGestureRecognizer){
        
        
        var profileUserId:String = ""
        
        let postObj = self.arrayCommunity[gesture.view!.tag]
        
        profileUserId = postObj.userID
        
        print("post id - \(postObj.postid)")
        self.pushUserProfileScreen(userId: profileUserId, currentUserId:self.currentUserId!)
        
    }
    
    @objc func moreInfoButtonAction(_sender:UIButton){
        
        print("\(_sender.tag)")
        print("\(_sender.superview?.tag ?? 0)")
        
        let postObj = self.arrayCommunity[_sender.tag]
        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Copy link", style: .default) { action -> Void in
            
            UIPasteboard.general.string = postObj.sharelink ?? ""
            
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Copy Link")
            
            print("First Action pressed")
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Share to", style: .default) { action -> Void in
            
            self.apiCallPostshare(postID: postObj.postid ?? "")
            
            let text = "Let me recommend you this application"
            let myWebsite = NSURL(string: postObj.sharelink ?? "")
            let shareAll = [text , myWebsite] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
            
            print("Second Action pressed")
        }
        
        let thirdAction: UIAlertAction = UIAlertAction(title: "Delete Post", style: .destructive) { action -> Void in
            
            let alert = UIAlertController(title: "Delete", message: "Are you sure want to delete a post?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "DELETE", style: .default, handler: { action in
                self.apiCallPostDelete(postID: postObj.postid ?? "")
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
            print("Second Action pressed")
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(thirdAction)
        
        actionSheetController.addAction(cancelAction)
        
        // present an actionSheet...
        // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
        
        actionSheetController.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
        
        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
        
    }
    
    func likePostApi(_notifyUserId:String, _postId:String, imgLike:UIImageView, countLabel:UILabel){
        
        
        let requestObj = LikePostRequest(_user_id: self.currentUserId!, _notify_user_id: _notifyUserId, _post_id: _postId)
        
        
        ApiCallManager.shared.apiCall(request: requestObj, apiType: .LIKE_POST, responseType: LikePostResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                
                
                print("like response - \(results.like)")
                
                self.changeLikeButtonIconAndCount(results: results, imgViewLike: imgLike, likeCountLabel: countLabel)
                
            }
            else{
                
                self.showAlertPopupWithMessage(msg: results.messages)
                
                
            }
            
            
            
        } failureHandler: { (error) in
            
            
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
}

extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        masksToBounds = false
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
