//
//  PostViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 01/11/21.
//

import UIKit
import SDWebImage
import UITextView_Placeholder
import Toast_Swift
import SVProgressHUD

class PostViewController: MasterViewController {
    
    @IBOutlet weak var imgPTo: UIImageView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postTextview: UITextView!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var graphButton: UIButton!
    @IBOutlet weak var imgButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    private var userID:String?
    
    //MARK:- UIViewcontroller lifecycle methods ---
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        postTextview.placeholder = "What's Happening?"
        postTextview.placeholderColor = UIColor.darkGray
        
        self.postButton.changeBorder(width: 1.0, borderColor: .white, cornerRadius: 10.0)
        
        self.postTextview.becomeFirstResponder()
        
        self.postTextview.textContainerInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 15)
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.userID = appDelegate.loginResponseData?.id
            
            imgPTo.sd_setImage(with: URL(string: "\(appDelegate.loginResponseData?.profileImg ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
        
        
    }
    
    //MARK:- Image  overriding ---
    
    override func sendImageOnly(img: UIImage, formate:String) {
        
        self.postImageView.image = img
        
    }
    
    override func sendFileURLAfterUpload(imgUrl: String, mediaType: MediaType) {
        
        print("img url - \(imgUrl)")
        print("media type - \(mediaType)")
        
    }
        
    //MARK:- UIButton action methods ----
    
    @IBAction func postButtonAction(_ sender: Any) {
        
        print("\(#function)")
        
        if self.postTextview.text == ""
        {
            self.view.makeToast("Enter The Valid Data")
        }
        else
        {
            
            let requestPost = AddPostRequest(_id: "", _user_id:self.userID! , _message: self.postTextview.text, _location: "Indore", _latitude: "22.2332", _longitude: "75.2323")
            
            if let postImage = self.postImageView.image{
                
                self.apiCallPostWithImage(postImage: postImage, requestPost: requestPost)
            }
            else{
                
                self.apiCallPostWithoutImage(requestPost:requestPost)
            }
            
        }
       
        
    }
    
    //MARK:- API call methods ----
    
    
    func apiCallPostWithoutImage(requestPost:AddPostRequest){
        
        SVProgressHUD.show()
        
        ApiCallManager.shared.apiCall(request: requestPost, apiType: .ADD_POST, responseType: AddPostResponse.self, requestMethod: .POST) { (results) in
            
            let result:AddPostResponse = results
            print("Result - \(result.messages) -- ")
            
            if result.status == 1 {
                
                SVProgressHUD.dismiss()
                
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
                
                
            }
            else{
                SVProgressHUD.dismiss()
                self.showAlertPopupWithMessage(msg: result.messages)
                
            }
            
            
        } failureHandler: { (error) in
            SVProgressHUD.dismiss()
            self.showErrorMessage(error: error)
            
        }
         
        
    }
    
    func apiCallPostWithImage(postImage:UIImage, requestPost:AddPostRequest){
        
        SVProgressHUD.show()
        
        ApiCallManager.shared.UploadImage(request: requestPost, apiType: .ADD_POST, uiimagePic: postImage, responseType: AddPostResponse.self) { (response) in
            
            let result:AddPostResponse = response
            print("Result - \(result.messages) -- ")
            
            if result.status == 1 {
                
                SVProgressHUD.dismiss()
                
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            else{
                
                SVProgressHUD.dismiss()
                self.showAlertPopupWithMessage(msg: result.messages)
                
            }
            
        } failureHandler: { (error) in
            SVProgressHUD.dismiss()
            self.showErrorMessage(error: error)
            
        }
        
    }
    
    @IBAction func grpahButtonAction(_ sender: Any) {
        
        print("\(#function)")
        
        self.showAlertCommingSoon()
    }
    
    @IBAction func locationButtonAction(_ sender: Any) {
        print("\(#function)")
        
        self.showAlertCommingSoon()
    }
    
    @IBAction func imgButtonAction(_ sender: Any) {
        
        print("\(#function)")
        
        self.fetchImages(sourceType: .savedPhotosAlbum)
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
