//
//  SharePostMessageTableCell.swift
//  TradersView
//
//  Created by Ankit Gabani on 25/01/22.
//

import UIKit

class SharePostMessageTableCell: UITableViewCell {
    
    
    @IBOutlet weak var imgPro: UIImageView!
    
    @IBOutlet weak var lblNAme: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    @IBOutlet weak var imgSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
