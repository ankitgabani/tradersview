//
//  SignupViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 17/10/21.
//

import UIKit

import Firebase
class SignupViewController: MasterViewController {
    
    
    //MARK:- UI Object declarations ---
    
    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var emailIDTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    
    
    
    
    //MARK:- UIViewcontroller lifecycle methods ---
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        
        self.uiChanges()
        
        self.nameTextfield.text = "ajeet sharma"
        self.userNameTextfield.text = "a168"
        self.emailIDTextfield.text = "a168887@gmail.com"
        self.phoneTextfield.text = "+919009241741"
        self.passwordTextfield.text = "qwerty123"
        self.confirmPasswordTextfield.text = "qwerty123"
        
    }
    
    //MARK:- UI Changes -----
    
    
    func uiChanges(){
        
        
        self.signupButton.changeBorder(width: 1.0, borderColor: .lightGray, cornerRadius: 25.0)
        
        self.nameTextfield.changeBorder(width: 0.0, borderColor: .lightGray, cornerRadius: 25.0)
        self.userNameTextfield.changeBorder(width: 0.0, borderColor: .lightGray, cornerRadius: 25.0)
        self.emailIDTextfield.changeBorder(width: 0.0, borderColor: .lightGray, cornerRadius: 25.0)
        self.phoneTextfield.changeBorder(width: 0.0, borderColor: .lightGray, cornerRadius: 25.0)
        self.passwordTextfield.changeBorder(width: 0.0, borderColor: .lightGray, cornerRadius: 25.0)
        self.confirmPasswordTextfield.changeBorder(width: 0.0, borderColor: .lightGray, cornerRadius: 25.0)
        
        
        self.nameTextfield.setLeftPaddingPoints(10.0)
        self.nameTextfield.setRightPaddingPoints(10.0)
        
        
        self.userNameTextfield.setLeftPaddingPoints(10.0)
        self.userNameTextfield.setRightPaddingPoints(10.0)
        
        self.emailIDTextfield.setLeftPaddingPoints(10.0)
        self.emailIDTextfield.setRightPaddingPoints(10.0)
        
        
        self.phoneTextfield.setLeftPaddingPoints(10.0)
        self.phoneTextfield.setRightPaddingPoints(10.0)
        
        self.passwordTextfield.setLeftPaddingPoints(10.0)
        self.passwordTextfield.setRightPaddingPoints(10.0)
        
        
        self.confirmPasswordTextfield.setLeftPaddingPoints(10.0)
        self.confirmPasswordTextfield.setRightPaddingPoints(10.0)
        
        
        
    }
    
    //MARK:- UIButton Action -----
    
    @IBAction func signupButtonAction(_ sender: Any) {
        
        
        if !self.isTextfieldEmpty(textFields: [self.nameTextfield, self.userNameTextfield, self.emailIDTextfield, self.phoneTextfield, self.passwordTextfield, self.confirmPasswordTextfield]){
            
            if self.passwordTextfield.text == self.confirmPasswordTextfield.text {
                
                
                self.callRegisterApi()
                
                
            }
            else{
                
                
                self.showAlertPopupWithMessage(msg: "Both password should be same.")
                
            }
            
            
        }
        
        
        
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- Add user entry in firebaes for chatting ---
    
    func addUserForChat(userData:TDUserRegisterUserdata, successHandler:@escaping()->Void, errorHandler:@escaping(Error)->Void){
        
        
        //create user dictionary to first entery in "user" node firebase --
        
        let channelGroup = [["groupid":"Null"]]
        let privateGroup = [["groupid":"Null"]]
        let publicGroup = [["groupid":"Null"]]
                
        let dict:[String:Any] = ["channel_group":channelGroup, "private_group":privateGroup, "public_group":publicGroup,"date":Date.getCurrentDate(), "email":userData.email ?? "", "psd":userData.username ?? "", "recent_message":"","status":"online", "userId":userData.id ?? "","username":userData.name ?? "", "imageURL":"https://spsofttech.com/projects/treader/images/dummy.png"]
        
        
        
        self.ref.child("user").child(userData.id).setValue(dict) { (error, reference) in
            
            
            if let err = error {
                
                errorHandler(err)
                
                
            }
            else{
                
                successHandler()
                
                
            }
        }
        
        
    }
    
    //MARK:- Api call for registration ----
    
    
    func callRegisterApi(){
     
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "name",
            "value":  self.nameTextfield.text!,
            "type": "text"
          ],
          [
            "key": "username",
            "value": self.userNameTextfield.text!,
            "type": "text"
          ],
          [
            "key": "email",
            "value": self.emailIDTextfield.text!,
            "type": "text"
          ],
          [
            "key": "mobile_no",
            "value": self.phoneTextfield.text!,
            "type": "text"
          ],
          [
            "key": "password",
            "value": self.passwordTextfield.text!,
            "type": "text"
          ],
          [
            "key": "facebook_id",
            "value":"",
            "type": "text"
          ],
          [
            "key": "google_id",
            "value": "",
            "type": "text"
          ],
          [
            "key": "device_type",
            "value": "iOS",
            "type": "text"
          ],
          [
            "key": "device_token",
            "value": "123456",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
                do {
                    let fileData = try NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                      + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
                catch
                {
                    print(error)
                }
              
            }
          }
        }
        body += "--\(boundary)--\r\n";
        
        let username = "admin"
        let password = "123"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://spsofttech.com/projects/treader/api/register")!,timeoutInterval: Double.infinity)
        request.addValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
            
            let dict = self.convertToDictionary(text: String(data: data, encoding: .utf8)!)
            
            let responseMsg = dict!["messages"] as? String
            let responsestatus = dict!["status"] as? Int
            
            if responsestatus == 1
            {
                DispatchQueue.main.async {
                    self.view.makeToast(responseMsg)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let arrUserdata = dict!["userdata"] as? NSArray
                        
                        let dicData = arrUserdata?[0] as! NSDictionary
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                        
                        
                        self.appDelegate.saveCurrentUserData(dic: TDUserRegisterUserdata(fromDictionary: dicData))
                        self.appDelegate.loginResponseData = TDUserRegisterUserdata(fromDictionary: dicData)
                        
                        self.addUserForChat(userData: self.appDelegate.loginResponseData!) {
                            self.appDelegate.mainNavigation = self.navigationController
                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
                            let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            let homeNavigation = UINavigationController(rootViewController: home)
                            homeNavigation.navigationBar.isHidden = true
                            self.appDelegate.window?.rootViewController = homeNavigation
                            self.appDelegate.window?.makeKeyAndVisible()

                        } errorHandler: { (error) in
                            
                        }

                        
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.view.makeToast(responseMsg)
                }
            }
            
           
             
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

        
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}


extension SignupViewController{
    
    
    func parseSuccessHandler(response: ResponseModel) {
        
        print("\(#function)")
        
        let registerResponse:RegisterResponse = response as! RegisterResponse
        
        if registerResponse.userdata == nil {
            
            self.showAlertPopupWithMessage(msg: registerResponse.messages)
            
        }
        else{
            
            self.showAlertPopupWithMessageWithHandler(msg: "Register Successfully!!") {
                
                self.navigationController?.popViewController(animated: true)
                
            }
            
            
        }
        
    }
    func parseErrorHandler(error: Error) {
        print("\(#function)")
        self.showErrorMessage(error: error)
    }
    
    func parseSomethingWentWrong() {
        print("\(#function)")
        self.showAlertSomethingWentWrong()
        
    }
}

extension SignupViewController:UITextFieldDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    
}
