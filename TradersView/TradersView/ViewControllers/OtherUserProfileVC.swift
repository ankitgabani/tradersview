//
//  OtherUserProfileVC.swift
//  TradersView
//
//  Created by Ankit Gabani on 26/02/22.
//

import UIKit

class OtherUserProfileVC: MasterViewController {
    
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imgPropic: UIImageView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var viewPreminum: UIView!
    @IBOutlet weak var btnPremium: UIButton!
    @IBOutlet weak var btnPremiCont: NSLayoutConstraint!
    @IBOutlet weak var lblUsername: UILabel!
    
    @IBOutlet weak var lblAccuracyCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblFollwersCount: UILabel!
    @IBOutlet weak var lblPostCount: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    var userIDOfProfile:String = ""
    private var currentUserId:String = ""
    var selectedIndex = 0

    private var myPostPageNumber:Int = 0
    private var communityPageNumber:Int = 0
    
    private var shouldStopMyPostLoadMore:Bool = false
    private var shouldStopCommunityLoadMore:Bool = false
    
    private var myTradersPageNumber:Int = 0
    private var TradersPageNumber:Int = 0
    
    private var shouldStopMyTradersLoadMore:Bool = false
    private var shouldStopTradersLoadMore:Bool = false
    
    var userProfileObj:GetProfileByIDDatum?
    
    var arrayMyPost:[GetPostListByUserIdResponseDatum] = []
    var arrayMyTrader:[TraderssResponseDatum] = []

    
    var scrollViewIndicatorLabel: UILabel!
        
    let refreshControl = UIRefreshControl()
    
    //   @IBOutlet weak var tableViewUserProfile: UITableView!
    
    //MARK:- UIViewcontroller lifecycle ---
    
    var otherUserProfileID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        lineView.layer.applySketchShadow(color: UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha: 0.7), alpha: 1, x: 0, y: 0, blur: 10, spread: 0)
        
        
        //        self.tableViewUserProfile.estimatedRowHeight = 88.0
        //        self.tableViewUserProfile.rowHeight = UITableView.automaticDimension
        //
        //        self.tableViewUserProfile.contentInset = .zero
        print("Current user id - \(self.currentUserId)")
        print("Profile id - \(self.userIDOfProfile)")
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            
            if self.navigationController?.viewControllers.count == 1{
                
                self.currentUserId = appDelegate.loginResponseData?.id ?? ""
                self.userIDOfProfile = appDelegate.loginResponseData?.id ?? ""
                
                
            }
            else{
                
                self.currentUserId = appDelegate.loginResponseData?.id ?? ""
                
                
            }
            
            self.callApiToFetchUserProfile()
            self.apiCallMyPost()
            self.apiCallMyTraders()
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
        
        
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        // self.tableViewUserProfile.addSubview(refreshControl)
        
        
        self.apiCall()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblView.reloadData()
    }
    
    
    //MARK:- Refresh apiCall ----
    
    @objc func refresh(_ sender: AnyObject) {
        
        self.myPostPageNumber = 0
        
        self.apiCall()
        
        
    }
    
    func apiCall(){
        self.callApiToFetchUserProfile()
        self.apiCallMyPost()
        self.apiCallMyTraders()
        
    }
    
    //MARK:- UIButton action methods ---
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        
        self.pushScreenWithScreenName(screenName: "SearchViewController", currentUserId: self.currentUserId)
        
        
    }
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        
        
        self.pushScreenWithScreenName(screenName: "NotificationViewController", currentUserId: self.currentUserId)
        
    }
    
    @objc func shareTradeButtonAction(sender:UIButton){
        
        self.showAlertCommingSoon()
        
        
    }
    
    @objc func backButtonAction(sender:UIButton){
        
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    //MARK:- API call methods -----
    
    
    func likePostApi(_notifyUserId:String, _postId:String, imgLike:UIImageView, countLabel:UILabel){
        
        
        let requestObj = LikePostRequest(_user_id: self.otherUserProfileID, _notify_user_id: self.currentUserId, _post_id: _postId)
        
        
        
        ApiCallManager.shared.apiCall(request: requestObj, apiType: .LIKE_POST, responseType: LikePostResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                print("like response - \(results.like)")
                
                
                
                self.changeLikeButtonIconAndCount(results: results, imgViewLike: imgLike, likeCountLabel: countLabel)
                
            }
            else{
                
                self.showAlertPopupWithMessage(msg: results.messages)
            }
            
            
        } failureHandler: { (error) in
            
            
            self.showErrorMessage(error: error)
            
        }
        
    }
    func callApiToFetchUserProfile(){
        
        
        let request = GetprofileByIdRequest(_user_id: self.otherUserProfileID, _id: self.currentUserId)
        
        
        ApiCallManager.shared.apiCall(request: request, apiType: .GET_PROFILE_BY_ID, responseType: GetProfileByIDResponse.self, requestMethod: .POST) { (results) in
            
            DispatchQueue.main.async {
                
                self.refreshControl.endRefreshing()
            }
            
            
            if results.status == 1 {
                
                
                if let userData = results.data{
                    
                    self.userProfileObj = userData
                    
                    self.imgPropic.sd_setImage(with: URL(string: "\(userData.profileImg ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
                    self.imgCover.sd_setImage(with: URL(string: "\(userData.coverImg ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))

                    DispatchQueue.main.async {
                        
                        self.lblUsername.text = userData.username ?? ""
                        
                        if userData.accuracy == ""
                        {
                            self.lblAccuracyCount.text = "0.0%"
                        }
                        else
                        {
                            self.lblAccuracyCount.text = "\(userData.accuracy ?? "0.0")%"
                        }
                        
                        self.lblFollowingCount.text = userData.following ?? ""
                        self.lblFollwersCount.text = userData.followers ?? ""
                        self.lblPostCount.text = "\(userData.post ?? 0)"
                        
                        let is_premium = userData.isPremium
                        
                        if is_premium == "1"
                        {
                            self.viewPreminum.isHidden = false
                            self.btnPremium.isHidden = false
                        }
                        else
                        {
                            self.viewPreminum.isHidden = true
                            self.btnPremium.isHidden = true
                        }
                    }
                    
                    if (self.userIDOfProfile != self.currentUserId) {
                        
                        DispatchQueue.main.async {
                            //   self.favImageView.isHidden = false
                            
                        }
                        if (self.userProfileObj?.favouriteProfile == 1)
                        {
                            
                            DispatchQueue.main.async {
                                
                                //  self.favImageView.image = UIImage(named: "fav-profile-filled")
                            }
                            
                            
                        }
                        else {
                            DispatchQueue.main.async {
                                //  self.favImageView.image = UIImage(named: "fav-profile-empty")
                            }
                        }
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            //  self.favImageView.isHidden = true
                        }
                        
                    }
                   
                    
                }
                else{
                    
                    
                    self.showAlertPopupWithMessageWithHandler(msg: "Data is not available for this user") {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
                
                
            }
            else {
                
                self.showAlertPopupWithMessageWithHandler(msg: results.messages) {
                    
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            
            DispatchQueue.main.async {
                
                self.refreshControl.endRefreshing()
            }
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
    
    func apiCallMyPost(){
         
        
        let request = GetPostListByUserIdRequest(_id: otherUserProfileID, _page: self.communityPageNumber)

        print("self.communityPageNumber - \(self.communityPageNumber)")
        
        ApiCallManager.shared.apiCall(request: request, apiType: .GET_POST_BY_USER_ID, responseType: GetPostListByUserIdResponse.self, requestMethod: .POST) { (results) in
             
            if results.status == 1{
                
                if let data = results.data{
                    
                    if self.communityPageNumber == 0 {
                        
                        self.shouldStopCommunityLoadMore = false
                        self.arrayMyPost = data
                    }
                    else{
                        
                        self.arrayMyPost.append(contentsOf: data)
                        
                    }
                    
                }
                else{
                    
                    if self.communityPageNumber == 0 {
                        
                        self.arrayMyPost.removeAll()
                        
                    }
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.tblView.reloadData()
                }
            }
            else {
                
                if self.communityPageNumber == 0 {
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                 //   self.showAlertPopupWithMessage(msg: results.messages)
                    
                }
                else{
                    
                    self.shouldStopCommunityLoadMore = true
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
    
    
    func apiCallMyTraders(){
         
        
        let request = TraderssRequest(_user_id: self.otherUserProfileID, _page: self.myPostPageNumber, visiter_user_id: self.currentUserId)

        print("self.myPostPageNumber - \(self.myPostPageNumber)")
        
        ApiCallManager.shared.apiCall(request: request, apiType: .GET_TRADE_BY_ID, responseType: TraderssResponse.self, requestMethod: .POST) { (results) in
             
            if results.status == 1{
                
                if let data = results.data{
                    
                    if self.myPostPageNumber == 0 {
                        
                        self.shouldStopTradersLoadMore = false
                        self.arrayMyTrader = data
                    }
                    else{
                        
                        self.arrayMyTrader.append(contentsOf: data)
                        
                    }
                    
                }
                else{
                    
                    if self.myPostPageNumber == 0 {
                        
                        self.arrayMyPost.removeAll()
                        
                    }
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.tblView.reloadData()
                }
            }
            else {
                
                if self.myPostPageNumber == 0 {
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                    self.showAlertPopupWithMessage(msg: results.messages)
                    
                }
                else{
                    
                    self.shouldStopMyTradersLoadMore = true
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
    
    //MARK:- UITapgesture ----
    
    @objc func likeImageViewTapGesture(gesture: UITapGestureRecognizer) {
        
        
        var postID:String = ""
        var notifyUserId:String = ""
        
        if gesture.view!.superview!.tag == 101{
            
            let postObj = self.arrayMyPost[gesture.view!.tag]
            postID = postObj.postid
            notifyUserId = postObj.userID
            
            print("post id - \(postObj.postid)")
            
        }
        
        
        let label = gesture.view!.superview!.subviews.compactMap({$0 as? UILabel})
        
        // print("label[0].text - \(label[0].text)")
        
        
        
        self.likePostApi(_notifyUserId: notifyUserId, _postId: postID, imgLike: (gesture.view as! UIImageView?)!,countLabel:label[0])
        
        
        
    }
    @objc func commentImageViewTapGesture(gesture: UITapGestureRecognizer) {
        
        
        var postID:String = ""
        var notifyUserId:String = ""
        
        
        if gesture.view!.superview!.tag == 101{
            
            let postObj = self.arrayMyPost[gesture.view!.tag]
            
            postID = postObj.postid
            notifyUserId = postObj.userID
            
            print("post id - \(postObj.postid)")
            
        }
        
        
        self.pushCommentScreen(postId: postID, notifyUserId: notifyUserId)
        
        
        
    }
    @objc func favProfileTapGestureAction(gesture: UITapGestureRecognizer){
        
        let request = FavProfileActionRequest(_id: self.currentUserId, _fav_id: self.otherUserProfileID)
        
        ApiCallManager.shared.apiCall(request: request, apiType: .FAV_PROFILE, responseType: FavProfileActionResponse.self, requestMethod: .POST) { (results) in
            
            
            if results.status == 0{
                
                self.showAlertPopupWithMessage(msg: results.messages)
            }
            else{
                
                self.callApiToFetchUserProfile()
            }
            
        } failureHandler: { (error) in
            
            self.showErrorMessage(error: error)
        }
        
        
        
    }
    @objc func shareImageViewTapGesture(gesture: UITapGestureRecognizer) {
        
        
        if gesture.view!.superview!.tag == 101{
            
            let postObj = self.arrayMyPost[gesture.view!.tag]
            print("post id - \(postObj.postid)")
            
        }
        
        
    }
    
    @objc func profilePicImageViewTapGesture(gesture:UITapGestureRecognizer){
        
        
        var profileUserId:String = ""
        
        
        if gesture.view!.superview!.tag == 101{
            
            let postObj = self.arrayMyPost[gesture.view!.tag]
            
            profileUserId = postObj.userID
            
            print("post id - \(postObj.postid)")
            
        }
        
        
        if profileUserId != self.otherUserProfileID{
            
            self.pushUserProfileScreen(userId: profileUserId, currentUserId:self.currentUserId)
            
            
        }
        
        
    }
    
    @objc func followersViewTapGesture(gesture:UITapGestureRecognizer){
        
        print("\(#function)")
        
        self.pushFollowerFollowingList(userId: self.currentUserId, currentUserId: self.otherUserProfileID, flag: "0")
        
        
    }
    
    @objc func followingViewTapGesture(gesture:UITapGestureRecognizer){
        
        print("\(#function)")
        
        self.pushFollowerFollowingList(userId: self.currentUserId, currentUserId: self.otherUserProfileID, flag: "1")
        
        
        
    }
    
    @objc func moreInfoButtonAction(_sender:UIButton){
        
        
        print("\(_sender.tag)")
        print("\(_sender.superview?.tag ?? 0)")
        self.showAlertCommingSoon()
        
        
    }
    
    @objc func followButtonAction(_sender:UIButton){
        
        
        
        let request = FollowRequest(_user_id: self.otherUserProfileID, _follow_id: self.currentUserId)
        
        ApiCallManager.shared.apiCall(request: request, apiType: .FOLLOW, responseType: FollowResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1{
                
                if results.messages == "Follow"{
                    
                    DispatchQueue.main.async {
                        
                        _sender.setTitle("Following", for: .normal)
                    }
                    
                    
                    
                }
                else if results.messages == "Unfollow"{
                    
                    DispatchQueue.main.async {
                        
                        _sender.setTitle("Follow", for: .normal)
                    }
                }
                self.callApiToFetchUserProfile()
                
            }
            else if results.status == 0 {
                
                
                self.showAlertPopupWithMessage(msg: results.messages)
            }
            
        } failureHandler: { (error) in
            
            self.showErrorMessage(error: error)
        }
        
        
        
    }
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedMyProfileTab(_ sender: Any) {
        
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: MessageTabViewController = mainStoryboard.instantiateViewController(withIdentifier: "MessageTabViewController") as! MessageTabViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedProfileSettingTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: SettingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedCreatePostTrades(_ sender: Any) {
        
    }
    
}

extension OtherUserProfileVC:UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - UITableView Metheod
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedIndex == 0
        {
            return  self.arrayMyPost.count
        }
        else
        {
            return  self.arrayMyTrader.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if selectedIndex == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellPostCommunity") as! CellPost
            
            if indexPath.row >= self.arrayMyPost.count{
                return UITableViewCell()
            }
            
            let obj = self.arrayMyPost[indexPath.row]
            
            cell.nameLabel.text = obj.username
            
            cell.dateLabel.text = self.changeDateFormateToDisplay(dateString: obj.date)
            cell.profilePicImageView.sd_setImage(with: URL(string: "\(obj.profileImg)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.img_Pro.sd_setImage(with: URL(string: "\(obj.profileImg)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))

            cell.postCaptionLabel.text = obj.message
            
                    
            cell.likeCountLabel.text = obj.like
            cell.commentCountLabel.text = obj.comment
            cell.shareCountLabel.text = obj.share
            
            cell.likeImageView.tag = indexPath.row
            cell.commentImageView.tag = indexPath.row
            cell.shareImageView.tag = indexPath.row
            
            
            cell.moreInfoButton.tag = indexPath.row
            
            cell.moreInfoButton.superview!.tag = tableView.tag
            cell.likeImageView.superview!.tag = tableView.tag
            cell.commentImageView.superview!.tag = tableView.tag
            cell.shareImageView.superview!.tag = tableView.tag
            
            
            cell.profilePicImageView.tag = indexPath.row
           
            cell.profilePicImageView.superview!.tag = tableView.tag
            
            
            cell.img_Pro.tag = indexPath.row
           
            cell.img_Pro.superview!.tag = tableView.tag
            
         //   cell.btnWriteCommnet.tag = tableView.tag
          //  cell.btnWriteCommnet.addTarget(self, action: #selector(commentImageViewBtn(_:)), for: .touchUpInside)

            
            cell.likeImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.likeImageViewTapGesture(gesture:))))
            cell.commentImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.commentImageViewTapGesture(gesture:))))
            cell.shareImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.shareImageViewTapGesture(gesture:))))
            
            cell.postImageView.changeBorder(width: 1.0, borderColor: .lightGray, cornerRadius: 10.0)
            
            cell.img_Pro.isUserInteractionEnabled = true
            
            cell.profilePicImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profilePicImageViewTapGesture(gesture:))))
            cell.img_Pro.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profilePicImageViewTapGesture(gesture:))))

            cell.moreInfoButton.addTarget(self, action: #selector(self.moreInfoButtonAction(_sender:)), for: .touchUpInside)
            
            
            if obj.isComment != 0 {
                cell.commentImageView.image = UIImage(named: "comment-filled")
            }
            else{
                
                cell.commentImageView.image = UIImage(named: "comment-empty")
            }
            
            if obj.isShare != 0 {
                cell.shareImageView.image = UIImage(named: "share")
            }
            else{
                
                cell.shareImageView.image = UIImage(named: "share-post")
            }
            
            if obj.isLike != 0 {
                cell.likeImageView.image = UIImage(named: "like-filled")
            }
            else{
                
                cell.likeImageView.image = UIImage(named: "like-empty")
            }
            
            if let imgVideo = obj.imageVideo{
                
                let imgObj = imgVideo[0]
                let imgUrl = imgObj.image
                
                
                switch imgUrl {
                case .integer(let intValue):
                    cell.heightPostImageView.constant = 0.0
                    
                case .string(let strUrl):
                    cell.heightPostImageView.constant = 270
                    cell.postImageView.sd_setImage(with: URL(string: "\(strUrl)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
                    
                }
                
            }
            else{
                
                cell.heightPostImageView.constant = 0.0
                
            }
            
            
            if self.arrayMyPost.count - 1 == indexPath.row{



                if !self.shouldStopCommunityLoadMore{

                    self.communityPageNumber = self.communityPageNumber + 1

                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {

                        self.apiCallMyPost()
                    }

                }
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TradesCell") as! TradesCell
            
//            if indexPath.row >= self.arrayMyTrader.count{
//                return UITableViewCell()
//            }
            
            let obj = self.arrayMyTrader[indexPath.row]
            
            cell.curruncyLabel.text = obj.symbol_name ?? ""
            
            cell.dateLabel.text = obj.date ?? ""
            
            cell.tradPriceLabel.text = "Trade Price  \(obj.trade_price ?? 0)"
            cell.tradeProfitLabel.text = "Take Profit  \(obj.take_profit ?? 0)"
            cell.stopLessLabel.text = "Stop Loss  \(obj.stop_loss ?? 0)"

            cell.timeLabel.text = "\(obj.time ?? "")"
            
            cell.timeLabel.text = "\(obj.time ?? "")"
            
            cell.lnlReason.text = obj.reason ?? ""
            
            let difference_a = obj.difference_a ?? 0
            
            let difference_pr = obj.difference_pr ?? 0.0
            
            
            cell.percentLabel.text = "\(difference_a)(\(difference_pr)%)"
            
            cell.starView.rating = Double(obj.rating ?? 0)
             
            if obj.is_rating == 0
            {
                cell.starView.isUserInteractionEnabled = true
            }
            else
            {
                cell.starView.isUserInteractionEnabled = false
               
            }
            
            cell.starView.didFinishTouchingCosmos = { rating in
                
                
            }

            
            if obj.status == "Close" || obj.status == "close"
            {
                cell.openLabel.textColor = .red
                cell.openLabel.text = "Close"
            }
            else
            {
                cell.openLabel.textColor = .green
                cell.openLabel.text = "Open"
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HomeHeaderView", owner: self, options: [:])?.first as! HomeHeaderView
        
        
        headerView.btnCom.addTarget(self, action: #selector(clickedOmnCom(_:)), for: .touchUpInside)
        
        headerView.btnMyFeed.addTarget(self, action: #selector(clickedOnMyFeed(_:)), for: .touchUpInside)
        
        headerView.btnCom.setTitle("POST", for: .normal)
        headerView.btnMyFeed.setTitle("TRADES", for: .normal)
        
        
        if selectedIndex == 0
        {
            headerView.btnCom.alpha = 1
            headerView.btnMyFeed.alpha = 0.5
            headerView.viewCom.isHidden = false
            headerView.viewMyFeed.isHidden = true
        }
        else
        {
            headerView.btnCom.alpha = 0.5
            headerView.btnMyFeed.alpha = 1
            headerView.viewCom.isHidden = true
            headerView.viewMyFeed.isHidden = false
        }
        
        return headerView
    }
    
    @objc func clickedOmnCom(_ sender: UIButton)
    {
        selectedIndex = 0
        communityPageNumber = 0
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id ?? ""
            self.apiCallMyPost()
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    @objc func clickedOnMyFeed(_ sender: UIButton)
    {
        selectedIndex = 1
        communityPageNumber = 0
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            self.currentUserId = appDelegate.loginResponseData?.id ?? ""
            self.apiCallMyPost()
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

}


