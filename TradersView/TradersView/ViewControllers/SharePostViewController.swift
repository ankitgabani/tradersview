//
//  SharePostViewController.swift
//  TradersView
//
//  Created by Ankit Gabani on 25/01/22.
//

import UIKit
import SDWebImage
import Firebase
import FirebaseFirestore

class SharePostViewController: MasterViewController, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate {
    
    @IBOutlet weak var btnSendMsg: UIButton!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnGroup: UIButton!
    @IBOutlet weak var lblDriveLine: UILabel!
    
    @IBOutlet weak var tblViewMessages: UITableView!
    @IBOutlet weak var tblViewGroup: UITableView!
    
    @IBOutlet weak var lblNoDataFeoundGroyp: UILabel!
    @IBOutlet weak var leadingcont: NSLayoutConstraint!
    
    var isScrollByButton = false
    
    fileprivate var chatUserList_VM = ChatUserListViewModel()
    
    var dicLoginResponseData:TDUserRegisterUserdata?
    
    var arrSelectedMessages = NSMutableArray()
    
    var arrAddSelectedData = ChatUserListViewModel()
    
    var arrRGroupDetailModel: [GroupDetailModel] = [GroupDetailModel]()
    var arrAddGroupSelectedData: [GroupDetailModel] = [GroupDetailModel]()
    
    var chat_VM =  MyChatViewModel()
    
    var arrayCommunity: CommunityResponseDatum?
    
    var selectedMessage = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainScrollView.delegate = self
        
        tblViewMessages.delegate = self
        tblViewMessages.dataSource = self
        
        tblViewGroup.delegate = self
        tblViewGroup.dataSource = self
        
        fetchUserList()
        fetchPublicGroup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            dicLoginResponseData = appDelegate.loginResponseData
            
        }
        else{
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
    }
    
    func fetchUserList(){
        
        self.chatUserList_VM.fetchChatUserList {
            
            print("--- Fetch chat user list ---")
            
            self.tblViewMessages.isHidden = self.chatUserList_VM.chatUserList.count > 0 ?  false :  true
            
            self.tblViewMessages.reloadData()
            
        }
        
    }
    
    func fetchPublicGroup(){
        
        self.chatUserList_VM.fetchPublicGroupList {
            
            print("--- Fetch fetchPublicGroupList ---")
            
            self.tblViewGroup.isHidden = self.chatUserList_VM.publicGroupList.count > 0 ?  false :  true
            self.lblNoDataFeoundGroyp.isHidden = self.chatUserList_VM.publicGroupList.count > 0 ?  true :  false
            
            if self.chatUserList_VM.publicGroupList.count > 0
            {
                
                for object in self.chatUserList_VM.publicGroupList
                {
                    let arrGroup = object.muteNotificationUsers
                    print(object.groupName)
                    for objData in arrGroup
                    {
                        if self.dicLoginResponseData?.id == objData.memberid
                        {
                            self.arrRGroupDetailModel.append(object)
                            print(objData.memberid)
                        }
                    }
                }
                
                self.tblViewGroup.reloadData()
            }
        }
    }
    
    //MARK: - UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblViewMessages
        {
            return self.chatUserList_VM.chatUserList.count
        }
        else
        {
            return self.arrRGroupDetailModel.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == tblViewMessages
        {
            let cell = tblViewMessages.dequeueReusableCell(withIdentifier: "SharePostMessageTableCell") as! SharePostMessageTableCell
            
            let searchUser = self.chatUserList_VM.chatUserList[indexPath.row]
            
            cell.imgPro.tag = indexPath.row
            
            cell.imgPro.sd_setImage(with: URL(string: "\(searchUser.imageURL)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            
            cell.lblUsername.text = searchUser.recentMessage
            cell.lblNAme.text = searchUser.username
            
            if arrSelectedMessages.contains(searchUser.userID)
            {
                cell.imgSelected.image = UIImage(named: "ic_selected")
            }
            else
            {
                cell.imgSelected.image = UIImage(named: "ic_unselected")
            }
            
            return cell
        }
        else
        {
            let cell = tblViewGroup.dequeueReusableCell(withIdentifier: "SharePostMessageTableCell") as! SharePostMessageTableCell
            
            let publicGroup = self.arrRGroupDetailModel[indexPath.row]
            
            cell.imgPro.tag = indexPath.row
            
            cell.imgPro.sd_setImage(with: URL(string: "\(publicGroup.profileImage)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            
            cell.lblUsername.text = publicGroup.recentMessage
            cell.lblNAme.text = publicGroup.groupName
            
            if arrSelectedMessages.contains(publicGroup.groupID ?? "")
            {
                cell.imgSelected.image = UIImage(named: "ic_selected")
            }
            else
            {
                cell.imgSelected.image = UIImage(named: "ic_unselected")
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblViewMessages
        {
            let cell = tblViewMessages.cellForRow(at: indexPath) as! SharePostMessageTableCell
            
            let searchUser = self.chatUserList_VM.chatUserList[indexPath.row]
            
            let userID = searchUser.userID
            
            if arrSelectedMessages.contains(userID)
            {
                arrSelectedMessages.remove(userID)
                //  arrAddSelectedData.chatUserList.remove(at: indexPath.row)
            }
            else
            {
                arrSelectedMessages.add(userID)
                arrAddSelectedData.chatUserList.append(searchUser)
            }
            
            tblViewMessages.reloadData()
        }
        else
        {
            let cell = tblViewGroup.cellForRow(at: indexPath) as! SharePostMessageTableCell
            
            let searchUser = self.arrRGroupDetailModel[indexPath.row]
            
            let groupID = searchUser.groupID ?? ""
            
            if arrSelectedMessages.contains(groupID)
            {
                arrSelectedMessages.remove(groupID)
                //  arrAddGroupSelectedData.remove(at: indexPath.row)
            }
            else
            {
                arrSelectedMessages.add(groupID)
                arrAddGroupSelectedData.append(searchUser)
            }
            
            tblViewGroup.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    
    //MARK: - Action Method
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedMessage(_ sender: Any) {
        isScrollByButton = true
        self.mainScrollView.scrollTo(horizontalPage: 0, verticalPage: 0, animated: true)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: []) {
            
            let indecatorWidth = self.view.frame.size.width - 80
            
            self.leadingcont.constant = CGFloat(0 * (Int(indecatorWidth)/2))
            
            print(self.leadingcont.constant)
            
            if self.leadingcont.constant == 0.0
            {
                self.btnGroup.alpha = 0.5
                self.btnMessage.alpha = 1
            }
            else
            {
                self.btnGroup.alpha = 1
                self.btnMessage.alpha = 0.5
            }
            
            self.view.layoutIfNeeded()
            
        } completion: { (isComplete) in
            print("Complete animation second scrollview")
        }
        
    }
    
    @IBAction func clickedPublic(_ sender: Any) {
        isScrollByButton = true
        self.mainScrollView.scrollTo(horizontalPage: 1, verticalPage: 0, animated: true)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: []) {
            
            let indecatorWidth = self.view.frame.size.width - 80
            
            self.leadingcont.constant = CGFloat(1 * (Int(indecatorWidth)/2))
            
            print(self.leadingcont.constant)
            
            if self.leadingcont.constant == 0.0
            {
                self.btnGroup.alpha = 0.5
                self.btnMessage.alpha = 1
            }
            else
            {
                self.btnGroup.alpha = 1
                self.btnMessage.alpha = 0.5
            }
            
            self.view.layoutIfNeeded()
            
        } completion: { (isComplete) in
            print("Complete animation second scrollview")
        }
        
    }
    
    @IBAction func clickedSendMessage(_ sender: Any) {
        
        for objectData in arrAddSelectedData.chatUserList
        {
            self.chat_VM.currentUserId = self.dicLoginResponseData?.id ?? ""
            self.chat_VM.otherUserId = objectData.userID
            
            let date = Timestamp().dateValue()
            
            // Create Date Formatter
            let dateFormatter = DateFormatter()
            
            // Set Date Format
            dateFormatter.dateFormat = "dd MMM yyyy HH:MM:SS"
            
            let textMsg:[String:Any] = ["groupId": objectData.userID, "message": self.arrayCommunity?.message ?? "", "message_type":"text", "profile_image": "", "sender_id":self.dicLoginResponseData?.id ?? "", "sender_user_name":self.dicLoginResponseData?.name ?? "", "timestamp":dateFormatter.string(from: date)]
            
            self.chat_VM.messageToBeSend = textMsg
            
            self.chat_VM.sendChat()
        }
        
        self.view.makeToast("Message Send Successfully")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    @IBAction func clickedSendGroup(_ sender: Any) {
        
        for objectData in arrAddGroupSelectedData
        {
            self.chat_VM.currentUserId = self.dicLoginResponseData?.id ?? ""
            self.chat_VM.otherUserId = objectData.groupID
            
            let date = Timestamp().dateValue()
            
            // Create Date Formatter
            let dateFormatter = DateFormatter()
            
            // Set Date Format
            dateFormatter.dateFormat = "dd MMM yyyy HH:MM:SS"
            
            let textMsg:[String:Any] = ["groupId": objectData.groupID, "message": self.arrayCommunity?.message ?? "", "message_type":"text", "profile_image": "", "sender_id":self.dicLoginResponseData?.id ?? "", "sender_user_name":self.dicLoginResponseData?.name ?? "", "timestamp":dateFormatter.string(from: date)]
            
            self.chat_VM.messageToBeSend = textMsg
            
            self.chat_VM.sendChat()
        }
        
        self.view.makeToast("Message Send Successfully")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    //MARK: - API call
    func apiCallSendMessage(objData :ChatUserModel){
        
        if let currentUserID = self.dicLoginResponseData?.id
        {
            
            let request = SendMesageActionRequest(group_id: "", user_id: objData.userID, message: objData.recentMessage)
            
            ApiCallManager.shared.apiCall(request: request, apiType: .SEND_MESSAGE, responseType: MuteActionResponse.self, requestMethod: .POST) { (results) in
                
                if results.status == 1{
                    
                }
                else if results.status == 0 {
                    
                    self.showAlertPopupWithMessage(msg: results.messages)
                }
                
            } failureHandler: { (error) in
                
                self.showErrorMessage(error: error)
            }
            
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        print(scrollView.currentPage)
        
        self.changeUIofTopOptionIndicator(tag: scrollView.tag)
        
    }
    
    func changeUIofTopOptionIndicator(tag:Int)
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: []) {
            
            let indecatorWidth = self.view.frame.size.width - 80
            
            self.leadingcont.constant = CGFloat(self.mainScrollView.currentPage * (Int(indecatorWidth)/2))
            
            print(self.leadingcont.constant)
            
            if self.leadingcont.constant == 0.0
            {
                self.btnGroup.alpha = 0.5
                self.btnMessage.alpha = 1
            }
            else
            {
                self.btnGroup.alpha = 1
                self.btnMessage.alpha = 0.5
            }
            
            self.view.layoutIfNeeded()
            
        } completion: { (isComplete) in
            print("Complete animation second scrollview")
        }
    }
    
}

