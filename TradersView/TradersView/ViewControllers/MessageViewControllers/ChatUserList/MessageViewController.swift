//
//  MessageTabViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 19/10/21.
//

import UIKit
import Firebase
import SDWebImage
class MessageTabViewController: MasterViewController {
    
    
    //MARK:- Autolayout declaration -----
    
    @IBOutlet weak var scrollViewGroup: UIScrollView!
    @IBOutlet weak var scrollViewMessages: UIScrollView!
    
    @IBOutlet weak var leadingScrollIndicator: NSLayoutConstraint!
    @IBOutlet weak var leadingGroupScrollIndicator: NSLayoutConstraint!
    
    @IBOutlet weak var lblNoDataPublicGorup: UILabel!
    @IBOutlet weak var lblNoDataPrivateGroup: UILabel!
    @IBOutlet weak var lblNoDataMessage: UILabel!
    @IBOutlet weak var lblNoDataChannel: UILabel!
    
    
    //MARK:- UI Object declaration -----
    
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var notificationButton: UIButton!
    
    @IBOutlet weak var messageButton: UIButton!
    
    @IBOutlet weak var groupButton: UIButton!
    
    @IBOutlet weak var channelButton: UIButton!
    
    private var currentUserId:String?
    
    @IBOutlet weak var tableViewChannel: UITableView!
    @IBOutlet weak var tableViewGroupPrivate: UITableView!
    @IBOutlet weak var tableViewGroupPublic: UITableView!
    @IBOutlet weak var tableViewMessage: UITableView!
    
    var currentUserData:TDUserRegisterUserdata?
    
    fileprivate var chatUserList_VM = ChatUserListViewModel()
    
    var arrRGroupDetailModel: [GroupDetailModel] = [GroupDetailModel]()
    var arrPrivateDetailModel: [GroupDetailModel] = [GroupDetailModel]()
    
    var arrChannelDetailModel: [GroupDetailModel] = [GroupDetailModel]()
    
    //MARK:- UIViewcontroller lifecycle -----
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        lblNoDataPublicGorup.isHidden = true
        lblNoDataPrivateGroup.isHidden = true
        lblNoDataMessage.isHidden = true
        lblNoDataChannel.isHidden = true
        
        self.ref = Database.database().reference()
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        if isUserLogin == true
        {
            
            self.currentUserData = appDelegate.loginResponseData
            
            self.currentUserId = appDelegate.loginResponseData?.id
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User Data is not available")
        }
        
        self.view.layoutIfNeeded()
        self.fetchUserList()
        self.fetchChannelList()
        self.fetchPublicGroup()
        self.fetchPrivateGroup()
    }
    
    
    //MARK:- Firebase operations ---
    
    func fetchPrivateGroup(){
        
        self.chatUserList_VM.fetchPrivateGroupList {
            
            
            print("--- Fetch fetchPrivateGroupList ---")
            
            self.tableViewGroupPrivate.isHidden = self.chatUserList_VM.privateGroupList.count > 0 ?  false :  true
            self.lblNoDataPrivateGroup.isHidden = self.chatUserList_VM.privateGroupList.count > 0 ?  true :  false
            self.arrPrivateDetailModel.removeAll()
            
            if self.chatUserList_VM.privateGroupList.count > 0
            {
                
                for object in self.chatUserList_VM.privateGroupList
                {
                    let arrGroup = object.groupIDS
                    print(object.groupName)
                    for objData in arrGroup
                    {
                        if self.currentUserData?.id == objData.memberid
                        {
                            self.arrPrivateDetailModel.append(object)
                            print(objData.memberid)
                        }
                    }
                }
                
                self.tableViewGroupPrivate.reloadData()
            }
            
            self.tableViewGroupPrivate.reloadData()
            
            
        }
        
    }
    func fetchPublicGroup()
    {
        
        self.chatUserList_VM.fetchPublicGroupList {
            
            print("--- Fetch fetchPublicGroupList ---")
            
            self.tableViewGroupPublic.isHidden = self.chatUserList_VM.publicGroupList.count > 0 ?  false :  true
            self.lblNoDataPublicGorup.isHidden = self.chatUserList_VM.publicGroupList.count > 0 ?  true :  false
            self.arrRGroupDetailModel.removeAll()
            
            if self.chatUserList_VM.publicGroupList.count > 0
            {
                
                for object in self.chatUserList_VM.publicGroupList
                {
                    let arrGroup = object.groupIDS
                    print(object.groupName)
                    for objData in arrGroup
                    {
                        if self.currentUserData?.id == objData.memberid
                        {
                            self.arrRGroupDetailModel.append(object)
                            print(objData.memberid)
                        }
                    }
                }
                
                self.tableViewGroupPublic.reloadData()
            }
            
            
            //            print("--- Fetch fetchPublicGroupList ---")
            //
            //            self.tableViewGroupPublic.isHidden = self.chatUserList_VM.publicGroupList.count > 0 ?  false :  true
            //
            //            self.tableViewGroupPublic.reloadData()
            
        }
        
    }
    
    func fetchChannelList(){
        
        
        self.chatUserList_VM.fetchChannelList {
            
            print("--- Fetch fetchPublicGroupList ---")
            
            self.tableViewChannel.isHidden = self.chatUserList_VM.channelList.count > 0 ?  false :  true
            self.lblNoDataChannel.isHidden = self.chatUserList_VM.channelList.count > 0 ?  true :  false
            self.arrChannelDetailModel.removeAll()
            
            if self.chatUserList_VM.channelList.count > 0
            {
                
                for object in self.chatUserList_VM.channelList
                {
                    let arrGroup = object.groupIDS
                    print(object.groupName)
                    for objData in arrGroup
                    {
                        if self.currentUserData?.id == objData.memberid
                        {
                            self.arrChannelDetailModel.append(object)
                            print(objData.memberid)
                        }
                    }
                }
                
                self.tableViewChannel.reloadData()
            }
            
            
            
            print("--- Fetch chat user list --- \(self.chatUserList_VM.channelList.count)")
            
            
            //            self.tableViewChannel.isHidden = self.chatUserList_VM.channelList.count > 0 ?  false :  true
            //
            //            self.tableViewChannel.reloadData()
            
            
        }
        
    }
    func fetchUserList(){
        
        self.chatUserList_VM.fetchChatUserList {
            
            
            print("--- Fetch chat user list ---")
            
            self.tableViewMessage.isHidden = self.chatUserList_VM.chatUserList.count > 0 ?  false :  true
            
            self.lblNoDataMessage.isHidden = self.chatUserList_VM.chatUserList.count > 0 ?  true :  false
            
            
            self.tableViewMessage.reloadData()
            
            
        }
        
        
    }
    
    //MARK:- UIButton action methods ----
    
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        if let userID = self.currentUserId{
            
            self.pushScreenWithScreenName(screenName: "SearchViewController", currentUserId: userID)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
        
    }
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        
        if let userID = self.currentUserId{
            
            self.pushScreenWithScreenName(screenName: "NotificationViewController", currentUserId: userID)
            
        }
        else{
            
            self.showAlertPopupWithMessage(msg: "User id is not available")
            
        }
    }
    
    @IBAction func messageButtonAction(_ sender: Any) {
        
        self.scrollViewMessages.scrollTo(horizontalPage: 0, verticalPage: 0, animated: true)
        
        
        
        self.changeUIofTopOptionIndicator(tag: 0)
        
    }
    
    @IBAction func groupButtonAction(_ sender: Any) {
        
        self.scrollViewMessages.scrollTo(horizontalPage: 1, verticalPage: 0, animated: true)
        self.changeUIofTopOptionIndicator(tag: 0)
    }
    
    @IBAction func channelButtonAction(_ sender: Any) {
        
        self.scrollViewMessages.scrollTo(horizontalPage: 2, verticalPage: 0, animated: true)
        self.changeUIofTopOptionIndicator(tag: 0)
        
    }
    
    @IBAction func privateGroupButtonAction(_ sender: Any) {
        
        self.scrollViewGroup.scrollTo(horizontalPage: 1, verticalPage: 0, animated: true)
        self.changeUIofTopOptionIndicator(tag: 1)
    }
    
    @IBAction func publicGroupButtonAction(_ sender: Any) {
        
        self.scrollViewGroup.scrollTo(horizontalPage: 0, verticalPage: 0, animated: true)
        self.changeUIofTopOptionIndicator(tag: 1)
        
    }
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedMyProfile(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: UserProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        
    }
    
    @IBAction func clickedProfileSettings(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        let home: SettingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.appDelegate.window?.rootViewController = homeNavigation
        self.appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedCreateGeoupChannel(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionCreateGroup = UIAlertAction(title: "Create Group", style: .default) { (action) in
            self.createGroupScreen()
        }
        
        let actionCreateChannel = UIAlertAction(title: "Create Channel", style: .default) { (action) in
            self.createChannelScreen()
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(actionCreateChannel)
        actionSheet.addAction(actionCreateGroup)
        actionSheet.addAction(actionCancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func createGroupScreen()
    {
        let storyBoardDashboard = UIStoryboard(name: "Chat", bundle: nil)
        let vc:CreateGroupViewController = storyBoardDashboard.instantiateViewController(identifier: "CreateGroupViewController") as! CreateGroupViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func createChannelScreen()
    {
        let storyBoardDashboard = UIStoryboard(name: "Chat", bundle: nil)
        let vc:CreateChannelViewController = storyBoardDashboard.instantiateViewController(identifier: "CreateChannelViewController") as! CreateChannelViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- UI Changes ----
    
    func changeUIofTopOptionIndicator(tag:Int){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if tag == 0 {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: []) {
                    self.leadingScrollIndicator.constant = CGFloat(CGFloat(self.scrollViewMessages.currentPage) * ((self.view.frame.size.width-80)/3))
                    self.view.layoutIfNeeded()
                } completion: { (isComplete) in
                    print("Complete animation first scrollview")
                }
                switch self.scrollViewMessages.currentPage {
                case 0:
                    self.messageButton.setTitleColor(.black, for: .normal)
                    self.groupButton.setTitleColor(UIColor(hexString: "#6D9FAE"), for: .normal)
                    self.channelButton.setTitleColor(UIColor(hexString: "#6D9FAE"), for: .normal)
                case 1:
                    self.messageButton.setTitleColor(UIColor(hexString: "#6D9FAE"), for: .normal)
                    self.groupButton.setTitleColor(.black, for: .normal)
                    self.channelButton.setTitleColor(UIColor(hexString: "#6D9FAE"), for: .normal)
                    
                case 2:
                    self.messageButton.setTitleColor(UIColor(hexString: "#6D9FAE"), for: .normal)
                    self.groupButton.setTitleColor(UIColor(hexString: "#6D9FAE"), for: .normal)
                    self.channelButton.setTitleColor(.black, for: .normal)
                    
                default:
                    print("Default")
                }
            }
            else if tag == 1{
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: []) {
                    
                    var indecatorWidth = self.view.frame.size.width - 80
                    
                    self.leadingGroupScrollIndicator.constant = CGFloat(self.scrollViewGroup.currentPage * (Int(indecatorWidth)/2))
                    
                    self.view.layoutIfNeeded()
                    
                } completion: { (isComplete) in
                    print("Complete animation second scrollview")
                }
                
                
            }
            
            
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MessageTabViewController:UIScrollViewDelegate{
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        print(scrollView.currentPage)
        
        self.changeUIofTopOptionIndicator(tag: scrollView.tag)
        
    }
    
    
}
extension MessageTabViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80.0
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100{
            
            
            let cell:MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as! MessageTableViewCell
            
            
            let searchUser = self.chatUserList_VM.chatUserList[indexPath.row]
            
            cell.profilePicture.sd_setImage(with: URL(string: "\(searchUser.imageURL)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.profilePicture.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 22.5)
            
            
            cell.onlineStatusLabel.isHidden = false
            cell.onlineStatusLabel.text = searchUser.status
            
            cell.lblLastMsg.text = searchUser.recentMessage
            
            if searchUser.status == "online" || searchUser.status == "Online"
            {
                cell.onlineStatusLabel.textColor = UIColor.green
            }
            else
            {
                cell.onlineStatusLabel.textColor = UIColor.darkGray
                
            }
            
            cell.profilePicture.tag = indexPath.row
            
            cell.userNameLabel.text = searchUser.username
            // cell.userNameLabel.text = "@\(searchUser.psd.capitalized)"
            
            
            if self.chatUserList_VM.checkUserIsSelected(userData: searchUser){
                
                cell.accessoryType = .checkmark
                
            }
            else{
                
                cell.accessoryType = .none
                
            }
            
            
            return cell
            
        }
        else if tableView.tag == 101 {
            
            
            let cell:GroupAndChannelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GroupAndChannelTableViewCell") as! GroupAndChannelTableViewCell
            
            let publicGroup = self.arrRGroupDetailModel[indexPath.row]
            
            cell.profilePicture.sd_setImage(with: URL(string: "\(publicGroup.profileImage)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            
            cell.profilePicture.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 22.5)
            
            cell.lblLastMeg.text = publicGroup.recentMessage
            cell.lblDate.text = publicGroup.timeDate
            
            cell.groupNameLabel.text = publicGroup.groupName
            
            return cell
            
        }
        else if tableView.tag == 102 {
            
            
            let cell:GroupAndChannelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GroupAndChannelTableViewCell") as! GroupAndChannelTableViewCell
            
            
            let privateGroupObj = self.arrPrivateDetailModel[indexPath.row]
            
            cell.profilePicture.sd_setImage(with: URL(string: "\(privateGroupObj.profileImage)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.profilePicture.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 22.5)
            
            cell.lblLastMeg.text = privateGroupObj.recentMessage
            cell.lblDate.text = privateGroupObj.timeDate
            
            cell.groupNameLabel.text = privateGroupObj.groupName
            
            return cell
            
        }
        else if tableView.tag == 103 {
            
            print("tableview tag == 103")
            let cell:GroupAndChannelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GroupAndChannelTableViewCell") as! GroupAndChannelTableViewCell
            
            
            let channelObj = self.arrChannelDetailModel[indexPath.row]
            
            cell.profilePicture.sd_setImage(with: URL(string: "\(channelObj.profileImage)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.profilePicture.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 22.5)
            
            cell.lblLastMeg.text = channelObj.recentMessage
            cell.lblDate.text = channelObj.timeDate
            
            cell.groupNameLabel.text = channelObj.groupName
            
            
            return cell
            
        }
        
        return UITableViewCell()
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView.tag == 100{
            print(self.chatUserList_VM.chatUserList.count)
            
            return self.chatUserList_VM.chatUserList.count
            
        }
        else if tableView.tag == 101{
            
            return self.arrRGroupDetailModel.count
            
            
        }
        else if tableView.tag == 102{
            
            return self.arrPrivateDetailModel.count
            
        }
        else if tableView.tag == 103{
            
            return self.arrChannelDetailModel.count
            
        }
        
        return 0
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView.tag == 100{
            
            
            let selectedUser = self.chatUserList_VM.chatUserList[indexPath.row]
            
            let chatScreenObj = MyChatScreenModel(currentUserImageUrl: (self.currentUserData?.profileImg)!, currentUserName: (self.currentUserData?.name)!, currentUserId: (self.currentUserData?.id)!, otherUserId: (selectedUser.userID), otherUserName: (selectedUser.username), otherUserImageUrl: selectedUser.imageURL, isGroupChat: false)
            
            
            self.pushChatScreen(dataObj: chatScreenObj, chatType: .PERSONAL)
            
        }
        else if tableView.tag == 101{
            
            let groupObj = self.arrRGroupDetailModel[indexPath.row]
            
            
            let chatScreenObj = MyChatScreenModel(currentUserImageUrl: (self.currentUserData?.profileImg)!, currentUserName: (self.currentUserData?.name)!, currentUserId: (self.currentUserData?.id)!, otherUserId:groupObj.groupID, otherUserName: (groupObj.groupName), otherUserImageUrl: groupObj.profileImage, isGroupChat: true, dicOtherGroupDetails: groupObj)
            
            self.pushChatScreen(dataObj: chatScreenObj, chatType: .PUBLIC_GROUP)
        }
        else if tableView.tag == 102{
            
            let groupObj = self.arrPrivateDetailModel[indexPath.row]
            
            
            let chatScreenObj = MyChatScreenModel(currentUserImageUrl: (self.currentUserData?.profileImg)!, currentUserName: (self.currentUserData?.name)!, currentUserId: (self.currentUserData?.id)!, otherUserId:groupObj.groupID, otherUserName: (groupObj.groupName), otherUserImageUrl: groupObj.profileImage, isGroupChat: true, dicOtherGroupDetails: groupObj)
            
            self.pushChatScreen(dataObj: chatScreenObj, chatType: .PRIVATE_GROUP)
        }
        else if tableView.tag == 103{
            
            let groupObj = self.arrChannelDetailModel[indexPath.row]
            
            
            let chatScreenObj = MyChatScreenModel(currentUserImageUrl: (self.currentUserData?.profileImg)!, currentUserName: (self.currentUserData?.name)!, currentUserId: (self.currentUserData?.id)!, otherUserId:groupObj.groupID, otherUserName: (groupObj.groupName), otherUserImageUrl: groupObj.profileImage, isGroupChat: true, dicOtherGroupDetails: groupObj)
            
            self.pushChatScreen(dataObj: chatScreenObj, chatType: .CHANNEL)
        }
        
        
        
        
        
    }
    
}
