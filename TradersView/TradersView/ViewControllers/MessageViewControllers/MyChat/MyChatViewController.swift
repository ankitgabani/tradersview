//
//  MyChatViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 21/11/21.
//

import UIKit
import AVKit
import Firebase
import FirebaseFirestore
import SDWebImage

enum ChatType{
    
    case PERSONAL
    case PUBLIC_GROUP
    case PRIVATE_GROUP
    case CHANNEL
    
}


class PlayerView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self;
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer;
    }
    
    var player: AVPlayer? {
        get {
            return playerLayer.player;
        }
        set {
            playerLayer.player = newValue;
        }
    }
}

class ChatTextRightCell: UITableViewCell{
    
    @IBOutlet weak var contentBlueView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    @IBOutlet weak var messageLabel: PaddingLabel!
    
    
}
class ChatTextLeftCell: UITableViewCell{
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    @IBOutlet weak var contentBlueView: UIView!
    @IBOutlet weak var messageLabel: PaddingLabel!
    
    
}
class ChatImageRightCell: UITableViewCell{
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var imageChatImageView: UIImageView!
    
}
class ChatImageLeftCell: UITableViewCell{
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var imageChatImageView: UIImageView!
    
    
}

class ChatVideoLeftCell: UITableViewCell{
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var videoView: PlayerView!
    
}
class ChatVideoRightCell: UITableViewCell{
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var videoView: PlayerView!
    
}

struct MyChatScreenModel{
    
    var currentUserImageUrl:String = "https://spsofttech.com/projects/treader/images/dummy.png"
    
    var currentUserName:String = "Ajeet Sharma"
    var currentUserId:String = ""
    
    
    var otherUserId:String = ""
    var otherUserName:String = ""
    var otherUserImageUrl:String = ""
    var isGroupChat:Bool = false
    var dicOtherGroupDetails: GroupDetailModel?
    
}

class MyChatViewController:  MasterViewController, UITextFieldDelegate
{
    
    @IBOutlet weak var chatTextUIView: UIView!
    
    @IBOutlet weak var bottomConstraintsOfBottomView: NSLayoutConstraint!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var imgOptionDot: UIImageView!
    @IBOutlet weak var optionButton: UIButton!
    
    @IBOutlet weak var lblMemberCount: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var tableViewMessages: UITableView!
    @IBOutlet weak var textViewChat: UITextView!
    
    
    @IBOutlet weak var viewReport: UIView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    
    @IBOutlet weak var viewSearchBar: UIView!
    @IBOutlet weak var searchHieghtCont: NSLayoutConstraint!
    
    
    var imgOtherUser: UIImageView?
    
    var chat_VM =  MyChatViewModel()
    
    var myChatScreenModelObj:MyChatScreenModel?
    
    var userProfileObj:GetProfileByIDDatum?
    
    var strBlock = ""
    var strMute = ""
    var strMuteNoti = "Unmute Notification"
    
    var isSearching : Bool = false
    
    //MARK:- UIViewcontroller delegate ----
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSearchBar.isHidden = true
        searchHieghtCont.constant = 0
        
        self.viewReport.isHidden = true
        
        img1.image = UIImage(named: "ic_selected")
        img2.image = UIImage(named: "ic_unselected")
        img3.image = UIImage(named: "ic_unselected")
        img4.image = UIImage(named: "ic_unselected")
        img5.image = UIImage(named: "ic_unselected")
        img6.image = UIImage(named: "ic_unselected")
        
        
        self.tableViewMessages.isHidden = true
        self.fetchMessages()
        self.fetchNewAddedMessage()
        
        self.imgPro.sd_setImage(with: URL(string: "\(myChatScreenModelObj?.otherUserImageUrl ?? "")"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
        
        
        self.headingLabel.text = "\(myChatScreenModelObj!.otherUserName.capitalized)"
        self.tableViewMessages.estimatedRowHeight = 80.0
        self.tableViewMessages.rowHeight = UITableView.automaticDimension
        
        self.chat_VM.isThisGroupChat = myChatScreenModelObj!.isGroupChat
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        self.chatTextUIView.changeBorder(width: 1.0, borderColor: UIColor(hexString: "#474571"), cornerRadius: 20.0)
        view.addGestureRecognizer(tap)
        
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)

    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        
        if txtSearch.text!.count > 0
        {
            isSearching = true
            
            self.chat_VM.messageListSearching.removeAll()
            
            // find matching item and add it to the searcing array
            for i in 0..<self.chat_VM.messageList.count {
                
                let listItem : Message = self.self.chat_VM.messageList[i]
                let barCode = listItem.message
                if barCode.lowercased().range(of: self.txtSearch.text!.lowercased()) != nil
                {
                    self.chat_VM.messageListSearching.append(listItem)
                }
            }
            
            self.tableViewMessages.reloadData()
        }
        else
        {
            self.chat_VM.messageListSearching.removeAll()
            isSearching = false
            self.tableViewMessages.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.chat_VM.chatType == .PERSONAL
        {
            callApiToFetchUserProfile()
        }
        else if self.chat_VM.chatType == .PRIVATE_GROUP
        {
            self.lblMemberCount.text = "\(myChatScreenModelObj!.dicOtherGroupDetails!.groupIDS.count ?? 0) Members"
            
            for objMute in myChatScreenModelObj!.dicOtherGroupDetails!.muteNotificationUsers
            {
                
                if self.myChatScreenModelObj!.currentUserId == objMute.memberid
                {
                    self.strMuteNoti = "Mute Notification"
                }
                
            }
        }
        else if self.chat_VM.chatType == .PUBLIC_GROUP
        {
            self.lblMemberCount.text = "\(myChatScreenModelObj!.dicOtherGroupDetails!.groupIDS.count ?? 0) Members"
            
            for objMute in myChatScreenModelObj!.dicOtherGroupDetails!.muteNotificationUsers
            {
                
                if self.myChatScreenModelObj!.currentUserId == objMute.memberid
                {
                    self.strMuteNoti = "Mute Notification"
                }
                
            }
        }
        else if self.chat_VM.chatType == .CHANNEL
        {
            self.lblMemberCount.text = "\(myChatScreenModelObj!.dicOtherGroupDetails!.groupIDS.count ?? 0) Members"
        }
        
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    //MARK:- View model request for messages --
    
    func fetchMessages(){
        
        self.chat_VM.currentUserId = self.myChatScreenModelObj!.currentUserId
        self.chat_VM.otherUserId = self.myChatScreenModelObj!.otherUserId
        
        self.chat_VM.fetchAllMessages {
            
            
            self.tableViewMessages.reloadData()
            
            if self.chat_VM.messageList.count > 0
            {
                self.tableViewMessages.scrollToRow(at: IndexPath(row: self.chat_VM.messageList.count - 1, section: 0), at: .bottom, animated: true)
            }
            
            debugPrint("Crash point ----")
            
            if self.chat_VM.messageList.count == 0{
                
                
                self.tableViewMessages.isHidden = true
            }
            else{
                
                self.tableViewMessages.isHidden = false
                
            }
            
        }
    }
    
    func fetchNewAddedMessage(){
        
        
        //        self.chat_VM.currentUserId = self.myChatScreenModelObj!.currentUserId
        //        self.chat_VM.otherUserId = self.myChatScreenModelObj!.otherUserId
        //
        //        self.chat_VM.fetchNewAddedMessage {
        //
        //
        //            self.tableViewMessages.reloadData()
        //
        //
        //        }
        
        
    }
    
    //MARK:- Image fetch actions ---
    
    
    
    //MARK:- Image url overriding ---
    
    
    override func sendFileURLAfterUpload(imgUrl: String, mediaType: MediaType) {
        
        print("img url - \(imgUrl)")
        print("media type - \(mediaType)")
        
        switch mediaType {
        case .VIDEO:
            self.sendMessaage(msg: imgUrl, messageType: "Video")
            
            
        case .IMAGE:
            self.sendMessaage(msg: imgUrl, messageType: "Image")
            
            
        }
    }
    
    
        
    //MARK: - UIButton action methods ------
    
    @IBAction func clickedCancelasdsa(_ sender: Any) {
        viewSearchBar.isHidden = true
           searchHieghtCont.constant = 0
        
        self.chat_VM.messageListSearching.removeAll()
                  isSearching = false
                  self.tableViewMessages.reloadData()
    }
    
    
    @IBAction func clickedReport1(_ sender: Any) {
        
        img1.image = UIImage(named: "ic_selected")
        img2.image = UIImage(named: "ic_unselected")
        img3.image = UIImage(named: "ic_unselected")
        img4.image = UIImage(named: "ic_unselected")
        img5.image = UIImage(named: "ic_unselected")
        img6.image = UIImage(named: "ic_unselected")
    }
    
    @IBAction func clickedReport2(_ sender: Any) {
        img1.image = UIImage(named: "ic_unselected")
        img2.image = UIImage(named: "ic_selected")
        img3.image = UIImage(named: "ic_unselected")
        img4.image = UIImage(named: "ic_unselected")
        img5.image = UIImage(named: "ic_unselected")
        img6.image = UIImage(named: "ic_unselected")
    }
    
    @IBAction func clickedReport3(_ sender: Any) {
        img1.image = UIImage(named: "ic_unselected")
        img2.image = UIImage(named: "ic_unselected")
        img3.image = UIImage(named: "ic_selected")
        img4.image = UIImage(named: "ic_unselected")
        img5.image = UIImage(named: "ic_unselected")
        img6.image = UIImage(named: "ic_unselected")
    }
    
    @IBAction func clickedReport4(_ sender: Any) {
        img1.image = UIImage(named: "ic_unselected")
        img2.image = UIImage(named: "ic_unselected")
        img3.image = UIImage(named: "ic_unselected")
        img4.image = UIImage(named: "ic_selected")
        img5.image = UIImage(named: "ic_unselected")
        img6.image = UIImage(named: "ic_unselected")
    }
    
    @IBAction func clickedReport5(_ sender: Any) {
        img1.image = UIImage(named: "ic_unselected")
        img2.image = UIImage(named: "ic_unselected")
        img3.image = UIImage(named: "ic_unselected")
        img4.image = UIImage(named: "ic_unselected")
        img5.image = UIImage(named: "ic_selected")
        img6.image = UIImage(named: "ic_unselected")
    }
    
    @IBAction func clickedReport6(_ sender: Any) {
        img1.image = UIImage(named: "ic_unselected")
        img2.image = UIImage(named: "ic_unselected")
        img3.image = UIImage(named: "ic_unselected")
        img4.image = UIImage(named: "ic_unselected")
        img5.image = UIImage(named: "ic_unselected")
        img6.image = UIImage(named: "ic_selected")
    }
    
    @IBAction func clickedCance(_ sender: Any) {
        viewReport.isHidden = true
    }
    
    @IBAction func clickedReposrtUe(_ sender: Any) {
        viewReport.isHidden = true
    }
    
    @IBAction func clickedOpenChatProfuile(_ sender: Any)
    {
        let storyBoardDashboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
        
        let vc:OtherUserProfileVC = storyBoardDashboard.instantiateViewController(identifier: "OtherUserProfileVC") as! OtherUserProfileVC
        vc.otherUserProfileID = myChatScreenModelObj?.otherUserId ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func optionButtonAction(_ sender: Any) {
        
        if self.chat_VM.chatType == .PERSONAL
        {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let actionPost = UIAlertAction(title: "REPORT", style: .destructive) { (action) in
                self.viewReport.isHidden = false
            }
            
            let actionTrade = UIAlertAction(title: self.strBlock, style: .default) { (action) in
                
            }
            
            let actionCreateGroup = UIAlertAction(title: "Clear Chat", style: .default) { (action) in
                let alert = UIAlertController(title: "Clear Chat?", message: "Are you sure want to clear chat?", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                    
                })
                alert.addAction(ok)
                let cancel = UIAlertAction(title: "CLEAR CHAT", style: .default, handler: { action in
                    self.chat_VM.RemoveAllChat()
                    self.chat_VM.messageList.removeAll()
                    self.chat_VM.messageListSearching.removeAll()
                    self.tableViewMessages.reloadData()
                })
                alert.addAction(cancel)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
            }
            
            let actionCreateChannel = UIAlertAction(title: self.strMute, style: .default) { (action) in
                
            }
            
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            
            actionSheet.addAction(actionPost)
            actionSheet.addAction(actionTrade)
            actionSheet.addAction(actionCreateGroup)
            actionSheet.addAction(actionCreateChannel)
            actionSheet.addAction(actionCancel)
            
            self.present(actionSheet, animated: true, completion: nil)
        }
        else if self.chat_VM.chatType == .PUBLIC_GROUP
        {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            if myChatScreenModelObj!.dicOtherGroupDetails?.addminId == self.chat_VM.currentUserId
            {
                
                let actionTrade = UIAlertAction(title: self.strMuteNoti, style: .default) { (action) in
                    
                }
                
                let actionCreateGroup = UIAlertAction(title: "Search Message", style: .default) { (action) in
                    self.viewSearchBar.isHidden = false
                    self.searchHieghtCont.constant = 38
                }
                
                let actionCreateChannel = UIAlertAction(title: "Clear Chat", style: .default) { (action) in
                    let alert = UIAlertController(title: "Clear Chat?", message: "Are you sure want to clear chat?", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                        
                    })
                    alert.addAction(ok)
                    let cancel = UIAlertAction(title: "CLEAR CHAT", style: .default, handler: { action in
                        self.chat_VM.RemoveAllChat()
                        self.chat_VM.messageList.removeAll()
                        self.chat_VM.messageListSearching.removeAll()
                        self.tableViewMessages.reloadData()
                    })
                    alert.addAction(cancel)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true)
                    })
                }
                
                let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                
                actionSheet.addAction(actionTrade)
                actionSheet.addAction(actionCreateGroup)
                actionSheet.addAction(actionCreateChannel)
                actionSheet.addAction(actionCancel)
            }
            else
            {
                let actionPost = UIAlertAction(title: "Leave Group", style: .default) { (action) in
                    
                }
                
                let actionTrade = UIAlertAction(title: self.strMuteNoti, style: .default) { (action) in
                    
                }
                
                let actionCreateGroup = UIAlertAction(title: "Search Message", style: .default) { (action) in
                    self.viewSearchBar.isHidden = false
                                        self.searchHieghtCont.constant = 38
                }
                
                let actionCreateChannel = UIAlertAction(title: "Clear Chat", style: .default) { (action) in
                    
                    let alert = UIAlertController(title: "Clear Chat?", message: "Are you sure want to clear chat?", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                        
                    })
                    alert.addAction(ok)
                    let cancel = UIAlertAction(title: "CLEAR CHAT", style: .default, handler: { action in
                        self.chat_VM.RemoveAllChat()
                        self.chat_VM.messageList.removeAll()
                        self.chat_VM.messageListSearching.removeAll()
                        self.tableViewMessages.reloadData()
                    })
                    alert.addAction(cancel)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true)
                    })
                    
                }
                
                let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                
                actionSheet.addAction(actionPost)
                actionSheet.addAction(actionTrade)
                actionSheet.addAction(actionCreateGroup)
                actionSheet.addAction(actionCreateChannel)
                actionSheet.addAction(actionCancel)
            }
            
            
            self.present(actionSheet, animated: true, completion: nil)
        }
        else if self.chat_VM.chatType == .PRIVATE_GROUP
        {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            if myChatScreenModelObj!.dicOtherGroupDetails?.addminId == self.chat_VM.currentUserId
            {
                
                let actionTrade = UIAlertAction(title: self.strMuteNoti, style: .default) { (action) in
                    
                }
                
                let actionCreateGroup = UIAlertAction(title: "Search Message", style: .default) { (action) in
                    self.viewSearchBar.isHidden = false
                                        self.searchHieghtCont.constant = 38
                }
                
                let actionCreateChannel = UIAlertAction(title: "Clear Chat", style: .default) { (action) in
                    let alert = UIAlertController(title: "Clear Chat?", message: "Are you sure want to clear chat?", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                        
                    })
                    alert.addAction(ok)
                    let cancel = UIAlertAction(title: "CLEAR CHAT", style: .default, handler: { action in
                        self.chat_VM.RemoveAllChat()
                        self.chat_VM.messageList.removeAll()
                        self.chat_VM.messageListSearching.removeAll()
                        self.tableViewMessages.reloadData()
                    })
                    alert.addAction(cancel)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true)
                    })
                }
                
                let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                
                actionSheet.addAction(actionTrade)
                actionSheet.addAction(actionCreateGroup)
                actionSheet.addAction(actionCreateChannel)
                actionSheet.addAction(actionCancel)
                
            }
            else
            {
                let actionPost = UIAlertAction(title: "Leave Group", style: .default) { (action) in
                    
                }
                
                let actionTrade = UIAlertAction(title: self.strMuteNoti, style: .default) { (action) in
                    
                }
                
                let actionCreateGroup = UIAlertAction(title: "Search Message", style: .default) { (action) in
                    self.viewSearchBar.isHidden = false
                                        self.searchHieghtCont.constant = 38
                }
                
                let actionCreateChannel = UIAlertAction(title: "Clear Chat", style: .default) { (action) in
                    let alert = UIAlertController(title: "Clear Chat?", message: "Are you sure want to clear chat?", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                        
                    })
                    alert.addAction(ok)
                    let cancel = UIAlertAction(title: "CLEAR CHAT", style: .default, handler: { action in
                        self.chat_VM.RemoveAllChat()
                        self.chat_VM.messageList.removeAll()
                        self.chat_VM.messageListSearching.removeAll()
                        self.tableViewMessages.reloadData()
                    })
                    alert.addAction(cancel)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true)
                    })
                }
                
                let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                
                actionSheet.addAction(actionPost)
                actionSheet.addAction(actionTrade)
                actionSheet.addAction(actionCreateGroup)
                actionSheet.addAction(actionCreateChannel)
                actionSheet.addAction(actionCancel)
                
            }
            
            
            self.present(actionSheet, animated: true, completion: nil)
        }
        else if self.chat_VM.chatType == .CHANNEL
        {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            
            let actionTrade = UIAlertAction(title: "Mute Notification", style: .default) { (action) in
                
            }
            
            let actionCreateGroup = UIAlertAction(title: "Search Message", style: .default) { (action) in
                self.viewSearchBar.isHidden = false
                                    self.searchHieghtCont.constant = 38
            }
            
            let actionCreateChannel = UIAlertAction(title: "Clear Chat", style: .default) { (action) in
                let alert = UIAlertController(title: "Clear Chat?", message: "Are you sure want to clear chat?", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                    
                })
                alert.addAction(ok)
                let cancel = UIAlertAction(title: "CLEAR CHAT", style: .default, handler: { action in
                    self.chat_VM.RemoveAllChat()
                    self.chat_VM.messageList.removeAll()
                    self.chat_VM.messageListSearching.removeAll()
                    self.tableViewMessages.reloadData()
                })
                alert.addAction(cancel)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
            }
            
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            
            actionSheet.addAction(actionTrade)
            actionSheet.addAction(actionCreateGroup)
            actionSheet.addAction(actionCreateChannel)
            actionSheet.addAction(actionCancel)
            
            self.present(actionSheet, animated: true, completion: nil)
        }
        
        
        
        //        DispatchQueue.main.async {
        //
        //            let dashBoardStoryBoard = UIStoryboard(name: "Chat", bundle: nil)
        //            let vc:ChannelDetailViewController = dashBoardStoryBoard.instantiateViewController(identifier: "ChannelDetailViewController")
        //
        //            vc.groupNameString = self.myChatScreenModelObj!.otherUserName
        //            vc.groupId = self.myChatScreenModelObj!.otherUserId
        //            vc.chatType = self.chat_VM.chatType
        //
        //            vc.messageList = self.chat_VM.messageList.filter({ (message) -> Bool in
        //
        //                if message.message_type == "Video" || message.message_type == "Image"{
        //
        //                    return true
        //                }
        //
        //                    return false
        //
        //            })
        //
        //            self.appDelegate.mainNavigation?.pushViewController(vc, animated: true)
        //
        //        }
        
    }
    
    
    @IBAction func takePhotoVideoAction(_ sender: Any) {
        
        bottomConstraintsOfBottomView.constant = 5
        view.endEditing(true)
        self.openCameraOptionActionsheet(shouldUploadOnFirebase: true, isVideo: true)
        
        
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        
        
        if self.textViewChat.text.trimmingCharacters(in: .whitespaces).count != 0 {
            
            self.sendMessaage(msg: self.textViewChat.text, messageType: "text")
            self.textViewChat.text = ""
            
        }
    }
    
    
    
    func sendMessaage(msg:String, messageType:String){
        
        
        let date = Timestamp().dateValue()
        
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        
        // Set Date Format
        dateFormatter.dateFormat = "dd MMM yyyy HH:MM:SS"
        
        let groupId = self.myChatScreenModelObj!.isGroupChat ? "\(self.chat_VM.otherUserId)" : "This is not group"
        
        let textMsg:[String:Any] = ["groupId":groupId, "message":msg, "message_type":messageType, "profile_image":self.myChatScreenModelObj!.currentUserImageUrl, "sender_id":self.myChatScreenModelObj!.currentUserId, "sender_user_name":self.myChatScreenModelObj!.currentUserName, "timestamp":dateFormatter.string(from: date)]
        
        self.chat_VM.messageToBeSend = textMsg
        
        
        self.chat_VM.sendChat()
        
        
    }
    
    //    func fetchMessages(){
    //
    //
    //        self.ref.child("UserMessage").child(self.currentUserId).child(otherUserId).queryOrderedByKey().observe(.value) { (snapshot) in
    //
    //            if  let dictResponse:[String:Any] = snapshot.value as? [String : Any]{
    //
    //                let totalUserKeysInchat = Array(dictResponse.keys)
    //                self.messageList.removeAll()
    //                print("Response -\(dictResponse)")
    //                for key in totalUserKeysInchat{
    //
    //                    print(key)
    //                    print(dictResponse[key])
    //                    print("Crash point -1")
    //
    //                    let msg = Message(dictionary: dictResponse[key] as! [String : Any])
    //
    //                    //  print(msg?.message)
    //                    print(dictResponse[key])
    //
    //                    self.messageList.append(msg!)
    //
    //
    //
    //                }
    //
    //                self.tableViewMessages.reloadData()
    //
    //
    //
    //
    //            }
    //
    //        }
    //
    //    }
    
    //MARK:- Keyboard notificaiton ---
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            bottomConstraintsOfBottomView.constant = keyboardSize.height - 35
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        bottomConstraintsOfBottomView.constant = 5
        
    }
    
    //MARK:- UIButton action methods ----
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: - API Calling
    func callApiToFetchUserProfile(){
        
        let request = GetprofileByIdRequest(_user_id: myChatScreenModelObj?.otherUserId ?? "", _id: self.chat_VM.currentUserId)
        
        
        ApiCallManager.shared.apiCall(request: request, apiType: .GET_PROFILE_BY_ID, responseType: GetProfileByIDResponse.self, requestMethod: .POST) { (results) in
            
            if results.status == 1 {
                
                if let userData = results.data{
                    
                    self.userProfileObj = userData
                    
                    DispatchQueue.main.async {
                        
                        if userData.isBlock == 1
                        {
                            self.strBlock = "Unblock"
                        }
                        else
                        {
                            self.strBlock = "Block"
                        }
                        
                        
                        if userData.isMute == 1
                        {
                            self.strMute = "Unmute"
                            
                        }
                        else
                        {
                            self.strMute = "Mute"
                        }
                        
                    }
                    
                }
                else{
                    
                    self.showAlertPopupWithMessageWithHandler(msg: "Data is not available for this user") {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
                
                
            }
            else {
                
                self.showAlertPopupWithMessageWithHandler(msg: results.messages) {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
            
            
        } failureHandler: { (error) in
            
            
            
            self.showErrorMessage(error: error)
            
        }
        
        
        
    }
    
}

extension MyChatViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
        if self.isSearching == true
        {
            return self.chat_VM.messageListSearching.count
        }
        else
        {
            return self.chat_VM.messageList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var msg: Message!
        
        if self.isSearching == true
        {
            msg = self.chat_VM.messageListSearching[indexPath.row]
        }
        else
        {
            msg = self.chat_VM.messageList[indexPath.row]
        }
        
        if msg.message_type == "text" && msg.sender_id == self.myChatScreenModelObj?.currentUserId{
            
            let cell:ChatTextRightCell = tableView.dequeueReusableCell(withIdentifier: "ChatTextRightCell") as! ChatTextRightCell
            
            cell.dateLabel.text = msg.timpstamp
            cell.userNameLabel.text = msg.sender_user_name.capitalized
            cell.profilePictureImageView.sd_setImage(with: URL(string: "\(msg.profile_image)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.messageLabel.text = msg.message
            cell.profilePictureImageView.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 12.5)
            cell.contentBlueView.changeBorder(width: 0.0, borderColor: .clear, cornerRadius: 10.0)
            
            
            return cell
        }
        else if msg.message_type == "text" && msg.sender_id != self.myChatScreenModelObj?.currentUserId{
            
            let cell:ChatTextLeftCell = tableView.dequeueReusableCell(withIdentifier: "ChatTextLeftCell") as! ChatTextLeftCell
            
            cell.dateLabel.text = msg.timpstamp
            cell.userNameLabel.text = msg.sender_user_name.capitalized
            cell.profilePictureImageView.sd_setImage(with: URL(string: "\(msg.profile_image)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.messageLabel.text = msg.message
            cell.profilePictureImageView.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 12.5)
            cell.contentBlueView.changeBorder(width: 0.0, borderColor: .clear, cornerRadius: 10.0)
            
            return cell
        }
        else  if msg.message_type == "Image" && msg.sender_id == self.myChatScreenModelObj?.currentUserId{
            
            let cell:ChatImageRightCell = tableView.dequeueReusableCell(withIdentifier: "ChatImageRightCell") as! ChatImageRightCell
            
            cell.dateLabel.text = msg.timpstamp
            cell.userNameLabel.text = msg.sender_user_name.capitalized
            cell.profilePictureImageView.sd_setImage(with: URL(string: "\(msg.profile_image)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.imageChatImageView.sd_setImage(with: URL(string: "\(msg.message)"), placeholderImage: UIImage(named: Constants.DEFAULT_POST_IMAGE))
            
            cell.profilePictureImageView.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 12.5)
            cell.imageChatImageView.changeBorder(width: 1.0, borderColor: .darkGray, cornerRadius: 8.0)
            
            
            return cell
        }
        else if msg.message_type == "Image" && msg.sender_id != self.myChatScreenModelObj?.currentUserId{
            
            let cell:ChatImageLeftCell = tableView.dequeueReusableCell(withIdentifier: "ChatImageLeftCell") as! ChatImageLeftCell
            
            cell.dateLabel.text = msg.timpstamp
            cell.userNameLabel.text = msg.sender_user_name.capitalized
            cell.profilePictureImageView.sd_setImage(with: URL(string: "\(msg.profile_image)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.imageChatImageView.sd_setImage(with: URL(string: "\(msg.message)"), placeholderImage: UIImage(named: "addImagePlaceHolder.png"))
            cell.profilePictureImageView.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 12.5)
            cell.imageChatImageView.changeBorder(width: 1.0, borderColor: .darkGray, cornerRadius: 8.0)
            
            return cell
        }
        else  if msg.message_type == "Video" && msg.sender_id == self.myChatScreenModelObj?.currentUserId{
            
            let cell:ChatVideoRightCell = tableView.dequeueReusableCell(withIdentifier: "ChatVideoRightCell") as! ChatVideoRightCell
            
            cell.dateLabel.text = msg.timpstamp
            cell.userNameLabel.text = msg.sender_user_name.capitalized
            cell.profilePictureImageView.sd_setImage(with: URL(string: "\(msg.profile_image)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            
            cell.profilePictureImageView.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 12.5)
            
            // cell.videoTitleLabel.text = "Sample Video" ;
            let url = NSURL(string: msg.message);
            let avPlayer = AVPlayer(url: url as! URL);
            cell.videoView?.playerLayer.player = avPlayer;
            
            
            return cell
        }
        else if msg.message_type == "Video" && msg.sender_id != self.myChatScreenModelObj?.currentUserId{
            
            let cell:ChatVideoLeftCell = tableView.dequeueReusableCell(withIdentifier: "ChatVideoLeftCell") as! ChatVideoLeftCell
            
            cell.dateLabel.text = msg.timpstamp
            cell.userNameLabel.text = msg.sender_user_name.capitalized
            cell.profilePictureImageView.sd_setImage(with: URL(string: "\(msg.profile_image)"), placeholderImage: UIImage(named: Constants.DEFAULT_PROFILE_PIC))
            cell.profilePictureImageView.changeBorder(width: 1.0, borderColor: .black, cornerRadius: 12.5)
            
            let url = NSURL(string: msg.message);
            let avPlayer = AVPlayer(url: url! as URL);
            cell.videoView?.playerLayer.player = avPlayer;
            
            
            return cell
        }
        
        
        
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let videoCell = (tableView.cellForRow(at: indexPath) as? ChatVideoRightCell) else { return }
        //  let visibleCells = tableView.visibleCells
        //        let minIndex = visibleCells.startIndex
        
        if videoCell.videoView.player?.timeControlStatus == AVPlayer.TimeControlStatus.playing{
            
            print("PAUSE")
            
            videoCell.videoView.player?.pause()
            videoCell.videoView.player?.seek(to: CMTime.zero)
            
        }
        else{
            print("PLAY")
            videoCell.videoView.player?.seek(to: CMTime.zero)
            
            videoCell.videoView.player?.play()
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return UITableView.automaticDimension
        
    }
    
    
}


