//
//  LoginViewController.swift
//  TradersView
//
//  Created by Ajeet Sharma on 17/10/21.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import Foundation
import Toast_Swift
import FBSDKLoginKit
import FBSDKCoreKit

class LoginViewController: MasterViewController,GIDSignInDelegate {
    
    //MARK:- UI Object declarations ---
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var emailIDTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    
    @IBOutlet weak var googleButton: UIButton!

    //MARK:- UIViewcontroller lifecycle methods ---
    
    var strName = ""
    var strEmail = ""
    var strPhone = ""
    var facebookID = ""
    var GoogleID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.loginButton.changeBorder(width: 1.0, borderColor: .lightGray, cornerRadius: 25.0)
        
        self.facebookButton.changeBorder(width: 1.0, borderColor: .lightGray, cornerRadius: 25.0)
        
        self.googleButton.changeBorder(width: 1.0, borderColor: .lightGray, cornerRadius: 25.0)
        
        
        self.emailIDTextfield.setLeftPaddingPoints(10.0)
        self.emailIDTextfield.setRightPaddingPoints(10.0)
        
        
        self.passwordTextfield.setLeftPaddingPoints(10.0)
        self.passwordTextfield.setRightPaddingPoints(10.0)
        
        
        self.emailIDTextfield.clearButtonMode = .always
        self.emailIDTextfield.clearButtonMode = .whileEditing
        
        
        self.emailIDTextfield.text = "fenil@gmail.com"
        self.passwordTextfield.text = "123456"
        self.imageLogo.changeBorder(width: 0.0, borderColor: .clear, cornerRadius: (Constants.screenWidth * 0.4)/2)
        
    }
    
    //MARK:- UIButton Action -----
    
    
    @IBAction func facebookButtonAction(_ sender: Any) {
        loginWithFacebook()
    }
    
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result!)
                    var strEmail = String()
                    if let email = (result as! NSDictionary).value(forKey: "email") as? String {
                        self.strEmail = email ?? ""
                    }
                    let strid = (result as! NSDictionary).value(forKey: "id") as! String
                    let name = (result as! NSDictionary).value(forKey: "name") as! String
                    let pictureDic = (result as! NSDictionary).value(forKey: "picture") as! NSDictionary
                    let dataDic = pictureDic.value(forKey: "data") as! NSDictionary
                    
                    let picture = dataDic.value(forKey: "url") as! String
                    
                    self.strName = name
                    self.facebookID = strid
                    
                    print(strEmail)
                    print(name)
                    
                    self.callRegisterApiNew()
                    
                } else {
                    print(error?.localizedDescription)
                }
            })
        }
    }
    
    //MARK:- Facebook Login
    func loginWithFacebook() {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { result, error in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
        
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        
        if !self.isTextfieldEmpty(textFields: [self.emailIDTextfield, self.passwordTextfield]){
            
           self.callLoginApi()
            
        }
        
        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func googlePlusButtonAction(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().signIn()
                
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {

        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        // let userId = user.userID                  // For client-side use only!
        self.GoogleID = user.userID!
        // let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        strEmail = email ?? ""
        strName = fullName ?? ""
        
        self.callRegisterApiNew()
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    //MARK:- Api call for registration ----
    
    func callLoginApi(){
       
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "username",
            "value": "",
            "type": "text"
          ],
          [
            "key": "email",
            "value": self.emailIDTextfield.text!,
            "type": "text"
          ],
          [
            "key": "password",
            "value": self.passwordTextfield.text!,
            "type": "text"
          ],
          [
            "key": "device_type",
            "value": "iOS",
            "type": "text"
          ],
          [
            "key": "device_token",
            "value": "123456",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
                
                do {
                    let fileData = try NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                      + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
                catch
                {
                    print(error)
                }
                
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let username = "admin"
        let password = "123"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()

        var request = URLRequest(url: URL(string: "https://spsofttech.com/projects/treader/api/login")!,timeoutInterval: Double.infinity)
        request.addValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
            
            let dict = self.convertToDictionary(text: String(data: data, encoding: .utf8)!)
            
            let responseMsg = dict!["messages"] as? String
            let responsestatus = dict!["status"] as? Int
            
            if responsestatus == 1
            {
                
                DispatchQueue.main.async {
                    self.view.makeToast(responseMsg)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let arrUserdata = dict!["userdata"] as? NSArray
                        
                        let dicData = arrUserdata?[0] as! NSDictionary
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                        
                        self.appDelegate.saveCurrentUserData(dic: TDUserRegisterUserdata(fromDictionary: dicData))
                        self.appDelegate.loginResponseData = TDUserRegisterUserdata(fromDictionary: dicData)
                        self.appDelegate.mainNavigation = self.navigationController
                        
                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
                        let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        let homeNavigation = UINavigationController(rootViewController: home)
                        homeNavigation.navigationBar.isHidden = true
                        self.appDelegate.window?.rootViewController = homeNavigation
                        self.appDelegate.window?.makeKeyAndVisible()
                        
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.view.makeToast(responseMsg)
                }
            }

            
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    
    
    func archiveObjectLoginData(userDataLogin:LoginUserData){
        
        
        do{
            let objData = try NSKeyedArchiver.archivedData(withRootObject: userDataLogin, requiringSecureCoding: true)
            UserDefaults.standard.set(objData, forKey: Constants.USER_DEFAULT_KEY_USER_DATA)
            UserDefaults.standard.synchronize()
            
            print("Archive Success ----")
            
        }catch (let error){
            #if DEBUG
            print("Failed to convert UIColor to Data : \(error.localizedDescription)")
            #endif
        }
    }
    
    func callRegisterApiNew()
    {

        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "name",
            "value":  self.strName,
            "type": "text"
          ],
          [
            "key": "username",
            "value": "Ankit Gabani AG",
            "type": "text"
          ],
          [
            "key": "email",
            "value": self.strEmail,
            "type": "text"
          ],
          [
            "key": "mobile_no",
            "value": self.strPhone,
            "type": "text"
          ],
          [
            "key": "password",
            "value": "123456",
            "type": "text"
          ],
          [
            "key": "facebook_id",
            "value": self.facebookID,
            "type": "text"
          ],
          [
            "key": "google_id",
            "value": self.GoogleID,
            "type": "text"
          ],
          [
            "key": "device_type",
            "value": "iOS",
            "type": "text"
          ],
          [
            "key": "device_token",
            "value": "123456",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
                do {
                    let fileData = try NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                      + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
                catch
                {
                    print(error)
                }
              
            }
          }
        }
        body += "--\(boundary)--\r\n";
        
        let username = "admin"
        let password = "123"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://spsofttech.com/projects/treader/api/register")!,timeoutInterval: Double.infinity)
        request.addValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
            
            let dict = self.convertToDictionary(text: String(data: data, encoding: .utf8)!)
            
            let responseMsg = dict!["messages"] as? String
            let responsestatus = dict!["status"] as? Int
            
            if responsestatus == 1
            {
                DispatchQueue.main.async {
                    self.view.makeToast(responseMsg)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let arrUserdata = dict!["userdata"] as? NSArray
                        
                        let dicData = arrUserdata?[0] as! NSDictionary
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                        
                        self.appDelegate.saveCurrentUserData(dic: TDUserRegisterUserdata(fromDictionary: dicData))
                        self.appDelegate.loginResponseData = TDUserRegisterUserdata(fromDictionary: dicData)
                        self.appDelegate.mainNavigation = self.navigationController
                        
                        
                        if responseMsg == "User Register SuccessFully Welcome"
                        {
                            self.addUserForChat(userData: self.appDelegate.loginResponseData!) {
                                let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
                                let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                let homeNavigation = UINavigationController(rootViewController: home)
                                homeNavigation.navigationBar.isHidden = true
                                self.appDelegate.window?.rootViewController = homeNavigation
                                self.appDelegate.window?.makeKeyAndVisible()
                            } errorHandler: { (error) in
                                
                            }
                        }
                        else
                        {
                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "DashboardFlow", bundle: nil)
                            let home: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            let homeNavigation = UINavigationController(rootViewController: home)
                            homeNavigation.navigationBar.isHidden = true
                            self.appDelegate.window?.rootViewController = homeNavigation
                            self.appDelegate.window?.makeKeyAndVisible()
                        }
                       
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.view.makeToast(responseMsg)
                }
            }
            
           
             
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    //MARK:- Add user entry in firebaes for chatting ---
    
    func addUserForChat(userData:TDUserRegisterUserdata, successHandler:@escaping()->Void, errorHandler:@escaping(Error)->Void){
        
        //create user dictionary to first entery in "user" node firebase --
        
        let channelGroup = [["groupid":"Null"]]
        let privateGroup = [["groupid":"Null"]]
        let publicGroup = [["groupid":"Null"]]
        
                
        let dict:[String:Any] = ["channel_group":channelGroup, "private_group":privateGroup, "public_group":publicGroup,"date":Date.getCurrentDate(), "email":userData.email ?? "", "psd":userData.username ?? "", "recent_message":"","status":"online", "userId":userData.id ?? "","username":userData.name ?? "", "imageURL":"https://spsofttech.com/projects/treader/images/dummy.png"]
        
        
        self.ref.child("user").child(userData.id).setValue(dict) { (error, reference) in
            
            
            if let err = error {
                
                errorHandler(err)
                
            }
            else{
                
                successHandler()
                
                
            }
        }
        
    }
    
}

extension LoginViewController:UITextFieldDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    
}

