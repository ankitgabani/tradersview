//
//  SetUpTwoFactorVC.swift
//  TradersView
//
//  Created by Ankit Gabani on 29/01/22.
//

import UIKit

class SetUpTwoFactorVC: UIViewController, OTPFieldViewDelegate {

    @IBOutlet weak var otpView: OTPFieldView!
    
    var otp = Int()
    var strCoustmOTP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupOtpView()
    }
    
    //MARK: - SET-UP OTPVIEW
    func setupOtpView()
    {
        self.otpView.fieldsCount = 4
        self.otpView.cursorColor = UIColor.black
        self.otpView.tintColor = UIColor.black
        self.otpView.displayType = .underlinedBottom
        self.otpView.fieldSize = 42
        self.otpView.separatorSpace = 20
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.initializeUI()
    }
    
    //MARK: - textfield delegate
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        self.strCoustmOTP = otpString
        // 87  authWithVerificationCode(otpString: otpString)
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedCrate(_ sender: Any) {
        
    }
    
}
